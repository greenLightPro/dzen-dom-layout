<?php
header("Content-Type: text/html; charset=utf-8");
require "PHPMailerAutoload.php";

function get_multy_checkbox ($check_arr) {
    if(isset($check_arr) && !empty($check_arr)) {
        $n = count($check_arr);
        $val = "";
        if($n > 0) {
            for ($i = 0; $i < $n; $i ++) {
                $val = $val . $check_arr[$i] . "; ";
            }
        }
        return $val;
    } else {
        return '';
    }
}

$f_from = $_POST['input_from'];
$f_name = $_POST['name'];
$f_phone = $_POST['phone'];
$f_email= $_POST['email'];

$f_city = $_POST['city'];

if (isset($f_city) && !empty($f_city) ) {
	require_once('../' . $f_city . '.php');
	global $city;
	$extra_email = $city['email'];
}


/*quiz*/
$f_building_repair = $_POST['building_repair']; /*Тип постройки*/
$f_flat_studio = $_POST['flat_studio']; // Студия
$f_count_rooms = $_POST['count_rooms']; // Количество комнат
$flat_square = $_POST['flat_square']; // Площадь квартиры
$state_premise = $_POST['state_premise']; // Состояние помещения
$replaning = $_POST['replaning']; // Перепланировка
$quiz_present = $_POST['quiz_present']; // Подарок
$quiz_send = $_POST['quiz_send']; // Средство связи

$f_region = $_POST['region'];
$f_exist_equipment = $_POST['exist_equipment'];
$f_app_send_result = $_POST['app_send_result'];
$f_type_check_example = get_multy_checkbox($_POST['type_check']);


$mail = new PHPMailer();
$mail->CharSet = 'UTF-8';
$mail->Subject="Заявка с сайта dzen-dom.ru";
$mail->addAddress("to@gmail.com");
if (isset($extra_email) && !empty($extra_email)) {
	$mail->addAddress($extra_email);
}

$mail->setFrom('from@mail.ru',"Дзен-Дом");
$message="
    <html lang='en'>
        <head>
            <meta charset='UTF-8'>
            <title>Заявка с сайта dzen-dom.ru</title>
        </head>
        <body>
            <table>
                <tbody>
                    <tr>";
if(isset($f_city) && !empty($f_city)) {
	$message.="<p>Город: $f_city</p>";
} else {
	$message.="<p>Город: saint_petersburg</p>";
}

if(isset($f_from) && !empty($f_from))
    $message.="<p>Место на сайте: $f_from</p>";
if(isset($f_name) && !empty($f_name))
    $message.="<p>Имя: $f_name</p>";
if(isset($f_email) && !empty($f_email))
    $message.="<p>Ваша почта: $f_email</p>";
if(isset($f_phone) && !empty($f_phone))
    $message.="<p>Телефон: $f_phone</p>";

/*quiz*/
if(isset($f_building_repair) && !empty($f_building_repair))
    $message.="<p>Тип постройки: $f_building_repair</p>";
if(isset($f_flat_studio) && !empty($f_flat_studio))
    $message.="<p>$f_flat_studio</p>";
if(isset($f_count_rooms) && !empty($f_count_rooms))
    $message.="<p>Количество комнат: $f_count_rooms</p>";
if(isset($flat_square) && !empty($flat_square))
    $message.="<p>Площадь квартиры: $flat_square&nbsp;м<sup>2</sup></p>";
if(isset($state_premise) && !empty($state_premise))
    $message.="<p>Состояние помещения: $state_premise</p>";
if(isset($replaning) && !empty($replaning))
    $message.="<p>Перепланировка: $replaning</p>";
if(isset($quiz_present) && !empty($quiz_present))
    $message.="<p>Подарок: $quiz_present</p>";
if(isset($quiz_send) && !empty($quiz_send))
    $message.="<p>Средство связи: $quiz_send</p>";



if (isset($_FILES['file']) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
    $extensions = array('jpeg', 'jpg', 'png', 'gif','pdf','doc','docx','xls','xlsx','rtf','txt','svg');
    $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
    if (in_array($ext, $extensions)){
        $mail->AddAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);
    }
}
$message.="</tr>
                </tbody>
            </table>
        </body>
     </html>";
$mail->msgHTML($message);
$m = $mail->send();
if ($m) {
    echo  "success";
}
else echo "error";