<section class="team_block">
    <div class="container">
        <div class="team">
            <p class="page_title wow fadeInUp">
                Обеспечивать клиентам<b> образцовое качество</b> и&nbsp;<b>превосходный  сервис</b>  невозможно без правильных людей в&nbsp;команде
            </p>
            <p class="subtitle wow fadeInUp">Знакомьтесь, ювелиры ремонтного цеха</p>

            <div class="team_slider_wrap slider_wrap slider_controls_pos wow fadeInUp">
                <div class="team_slider">

                    <?php $experts = [
					    [
                            'name' => 'Алексей Филютович',
                            'spec' => 'Старший Прораб',
                            'src' => 'expert_6.png'
                        ],
                        [
                            'name' => 'Сергей Федоров',
                            'spec' => 'Ювелир инженерных сетей',
                            'src' => 'expert_1.png'
                        ],
                        [
                            'name' => 'Федор Кирсон',
                            'spec' => 'Архитектор',
                            'src' => 'expert_2.png'
                        ],
                        [
                            'name' => 'Геннадий Фоменко',
                            'spec' => 'Бригадир Маляров',
                            'src' => 'expert_3.png'
                        ],
                        [
                            'name' => 'Михаил Копылов',
                            'spec' => 'Прораб с&nbsp;2-мя высшими',
                            'src' => 'expert_13.png'
                        ],
                        [
                            'name' => 'Александр',
                            'spec' => 'Сантехник каких мало',
                            'src' => 'expert_4.png'
                        ],
                        [
                            'name' => 'Жанна Яновская',
                            'spec' => 'Менеджер проектов',
                            'src' => 'expert_5.png'
                        ],
                        [
                            'name' => 'Егор Ниценко',
                            'spec' => 'Классный Маляр',
                            'src' => 'expert_7.png'
                        ],
                        [
                            'name' => 'Леонид',
                            'spec' => 'Первоклассный электрик',
                            'src' => 'expert_8.png'
                        ],
                        [
                            'name' => 'Елена Новицкая',
                            'spec' => 'Непревзойденный штукатур',
                            'src' => 'expert_9.png'
                        ],
                        [
                            'name' => 'Денис Правдинский',
                            'spec' => 'Руководитель отдела качества',
                            'src' => 'expert_10.png'
                        ],
                        [
                            'name' => 'Марат',
                            'spec' => 'Бригадир штукатуров',
                            'src' => 'expert_11.png'
                        ],
                        [
                            'name' => 'Камиль',
                            'spec' => 'Отделочник по призванию',
                            'src' => 'expert_12.png'
                        ],

                    ];

                    foreach ($experts as $expert) :
                    ?>

                        <div class="team_slide">
                            <div class="team_unit">
                                <div class="cover">
                                    <img src="img/empty.svg" alt="" data-lazy="img/experts/<?php echo($expert['src'])?>">
                                </div>
                                <div class="text">
                                    <p class="name"><?php echo($expert['name'])?></p>
                                    <p class="spec"><?php echo($expert['spec'])?></p>
                                </div>
                            </div>
                        </div>

                    <?php
                        endforeach;
                    ?>
                </div>

                <div class="btn_simple_slider btn_slider prev">
                    <img src="img/slider_arrow_light.svg" alt="">
                </div>
                <div class="btn_simple_slider btn_slider next">
                    <img src="img/slider_arrow_light.svg" alt="">
                </div>

            </div>
        </div>
    </div>
</section>