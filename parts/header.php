<meta charset="utf-8">
<title>ДЗЕН-ДОМ | Ремонт квартир без нервов</title>
<meta name="description" content="Ремонт квартир без задержек и переплат. Экспертно, надежно, с гарантией."/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="css/jquery.scrollbar.css" type="text/css">
<link rel="stylesheet" href="css/main.css" type="text/css">
<link rel="stylesheet" href="css/lightgallery.css" type="text/css">
<link rel="stylesheet" href="css/animate.css" type="text/css">
<link rel="stylesheet" href="css/fonts.css" type="text/css">

<?php
$path = "https://dzen-dom.ru";
?>

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $path;?>/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $path;?>/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path;?>/favicons/favicon-16x16.png">
<link rel="manifest" href="<?php echo $path;?>/favicons/site.webmanifest">
<link rel="mask-icon" href="<?php echo $path;?>/favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffcb00">
<meta name="theme-color" content="#ffffff">

<meta property="og:description" content="">
<meta property="og:image" content="<?php echo $path;?>/screenshot.jpg">
<meta property="og:site_name" content="dzen-dom.ru">
<meta property="og:title" content="ДЗЕН-ДОМ | Ремонт квартир без нервов">
<meta property="og:type" content="website">
<meta property="og:url" content="http://dzen-dom.ru">


