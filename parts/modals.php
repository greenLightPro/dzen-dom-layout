<div class="modal_overlay" id="modal_overlay"></div>

<div class="modal_wrapper" id="modal_get_consultation">
    <div class="modal_block modal_simple_block bg_purple_modal">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>

        <p class="simple_modal_title">
            Давайте <b>уточним <br>
            детали</b> по телефону?
        </p>
        <form class="page_form send_form">
            <div class="input_item">
                <label class="input _dark"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
            </div>
            <div class="input_item">
                <label class="input _dark"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
            </div>
            <div class="btn_wrap">
                <button class="btn_submit btn_light"><span class="btn_text">Получить консультацию</span></button>
            </div>
            <input type="hidden" name="from_where" class="input_where" value="Модальное окно: Получить консультацию">
        </form>
    </div>
</div>

<div class="modal_wrapper" id="modal_get_booking">
    <div class="modal_block modal_simple_block bg_purple_modal">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>

        <p class="simple_modal_title">
            Давайте <b>уточним <br> дату и время</b> <br> по телефону?
        </p>
        <form class="page_form send_form">
            <div class="input_item">
                <label class="input _dark"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
            </div>
            <div class="input_item">
                <label class="input _dark"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
            </div>
            <div class="btn_wrap">
                <button class="btn_submit btn_light"><span class="btn_text">Записаться</span></button>
            </div>
            <input type="hidden" name="from_where" class="input_where" value="Модальное окно: Записаться, мы уточним дату и время по телефону">
        </form>
    </div>
</div>

<div class="modal_wrapper" id="modal_get_order">
    <div class="modal_block modal_simple_block bg_purple_modal">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>

        <p class="simple_modal_title">
            Оставьте <b>заявку <br>
            на выезд</b> прораба
        </p>
        <p class="simple_modal_subtitle">
            Детали уточним по телефону
        </p>
        <form class="page_form send_form">
            <div class="input_item">
                <label class="input _dark"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
            </div>
            <div class="input_item">
                <label class="input _dark"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
            </div>
            <div class="btn_wrap">
                <button class="btn_submit btn_light"><span class="btn_text">Оставить заявку</span></button>
            </div>
            <input type="hidden" name="from_where" class="input_where" value="Модальное окно: Оставить заявку на выезд прораба">
        </form>
    </div>
</div>

<div class="modal_wrapper" id="modal_get_bonus">
    <div class="modal_block modal_bonus">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>

        <div class="modal_bonus_box">
            <div class="modal_bonus_content">
                <p class="title">Заберите ваши подарки и бонусы!</p>
                <p class="subtitle">Бонусы:</p>
                <div class="my_bonuses_box">
				<!--
                    <div class="item">
                        <div class="bonus_unit">
                            <div class="icon"><img src="img/get_icon_1.png" alt=""></div>
                            <div class="text">
                                <p>Сертификат <br> в “Петрович” <br> на 5000р</p>
                            </div>
                        </div>
                    </div>
				-->
                    <div class="item">
                        <div class="bonus_unit">
                            <div class="icon"><img src="img/get_icon_2.png" alt=""></div>
                            <div class="text">
                                <p>Скидку <br> на работы <br> 7%</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bonus_unit">
                            <div class="icon"><img src="img/get_icon_3.png" alt=""></div>
                            <div class="text"><p>Разработку технического проекта</p></div>
                        </div>
                    </div>
                </div>

                <p class="subtitle">+Один из трёх подарков на&nbsp;выбор</p>

                <div class="my_opt_bonuses_box">
                    <div class="item">
                        <div class="opt_bonus_unit">
                            <div class="icon"><img src="img/present_1.jpg" alt=""></div>
                            <div class="text">
                                <p>
                                    1 тонна <br>
                                    штукатурки
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="opt_bonus_unit">
                            <div class="icon"><img src="img/present_2.jpg" alt=""></div>
                            <div class="text">
                                <p>
                                    Натяжной <br>
                                    потолок в одной <br>
                                    комнате
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="opt_bonus_unit">
                            <div class="icon _contain"><img src="img/present_3.jpg" alt=""></div>
                            <div class="text">
                                <p>Робот пылесос</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal_bonus_side">
                <p class="simple_modal_title">
                    Мы закрепим <br>
                    <b>бонусы и подарки</b> <br>
                    за вашим номером
                </p>

                <form class="page_form send_form">
                    <div class="input_item">
                        <label class="input _dark"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                    </div>
                    <div class="btn_wrap">
                        <button class="btn_submit btn_light"><span class="btn_text">Оставить заявку</span></button>
                    </div>
                    <input type="hidden" name="from_where" class="input_where" value="Модальное окно: Заберите Ваши заявки и бонусы">
                </form>
                
                <div class="present_img_layer">
                    <img src="img/present_box.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal_wrapper" id="modal_send">
    <div class="modal_block modal_send">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>

        <div class="send_sun_light_layer">
            <div class="img">
                <img src="img/modal_sun_spot.png" alt="">
            </div>
        </div>
        <div class="send_lazy_men_layer">
            <img src="img/lazy_man_1.png" alt="" class="_desktop">
            <img src="img/lazy_man_1_mobile.png" alt="" class="_mobile">
        </div>

        <div class="modal_send_inner">
            <div class="main_master_unit">
                <div class="icon"><img src="img/popup_master.png" alt=""></div>
                <div class="text">
                    <p class="name">Алексей Филютович</p>
                    <p class="spec">
                        Главный строитель <br>
                        ДЗЕН-ДОМ
                    </p>
                </div>
            </div>
            <div class="content">
                <div class="main_master_text">
                    <p>
                        Прекрасно! Я уже занимаюсь <br>
						вашей заявкой. Можно <br>
                        закрыть сайты конкурентов <br>
                        и помечтать о том как вскоре <br>
                        преобразится ваше жилье
                    </p>
                    <p>
                        <b>Вы уже видели наши аккаунты <br>
                            в соцсетях?</b> Стараемся писать <br>
                        только с пользой и по делу… <br>
                        почти всегда)
                    </p>
                </div>
                
                <div class="socials_modal_box">
                    <div class="item">
                        <a href="https://vk.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/vk_icon.svg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="https://www.instagram.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/instagram_icon.svg" alt=""></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal_wrapper" id="modal_get_plan">
    <div class="modal_block modal_plan_block lg_box">
        <div class="btn_simple_close modal_close"><div class="inner"></div></div>
        <div class="plan_modal_content"> </div>
    </div>
</div>












