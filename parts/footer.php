<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/ui-touch-punch-min.js"></script>
<script src="js/jquery.scrollbar.min.js"></script>
<script src="js/jquerymask.js"></script>
<script src="js/lightgallery.js"></script>
<script src="js/lg-video.js"></script>
<script src="js/wow.min.js"></script>

<script src="js/slick.min.js"></script>
<script src="js/modals.js"></script>
<script src="js/quiz.js"></script>
<script src="js/app.js"></script>
<script src="js/plans_slider.js"></script>
