<section class="geography_block">
    <div class="bg_color_layer"></div>
    <div class="container">
        <div class="geography">
            <div class="geography_inner  wow fadeInLeft">
                <p class="title">Мы успешно сдали</p>
                <div class="geography_count">
                    <p class="digit">182</p>
                    <p class="desc">квартиры</p>
                </div>
                <p class="title _item_2">в 24 жилых <br> комплексах</p>
            </div>
            <div class="geography_img_layer  wow fadeIn"><img src="img/building.png" alt=""></div>
        </div>
    </div>

    <div class="map_block wow fadeIn">
        <div class="map_inner"><div id="map"></div></div>
    </div>
</section>

<script>
    var other_infowindows = false;
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat: 59.953194708253406, lng: 30.31789032806396},
            disableDefaultUI: true,
            mapTypeControl: false,
            scrollwheel:false,
            zoomControl:true,
            fullscreenControl:true,
            styles: [
                {
                    "featureType": "all",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#14505f"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#357c8e"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#dfe5e9"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#86a8b5"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#dfe5e9"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#b8c9d2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#bfd0d7"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": "-24"
                        },
                        {
                            "lightness": "-55"
                        },
                        {
                            "gamma": "0.00"
                        },
                        {
                            "color": "#357c8e"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#cbd9de"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ],
        });

        var infowindow = new google.maps.InfoWindow();
        var markers = setMarkers(map);
        for (var i = 0; i < markers.length; i++) {
            let my_marker = markers[i];

            google.maps.event.addListener(my_marker, 'click', function() {
                infowindow.setContent(my_marker['title']);
                infowindow.open(map, this);
            });
        }
    }

    var comp_objects = [
        ['ЖК Эврика', 60.04630970098679, 30.404814193115232, 1],
        ['ЖК Пять звезд', 59.962681011593546,30.403911999999938, 1],
        ['ЖК Усадьба на Ланском', 59.99353600651763,30.35661999999994, 1],
        ['ЖК Новелла', 59.98690706410976,30.297506499999972, 1],
        ['ЖК Мироздание', 59.96203456413934,30.328264999999956, 1],
        ['Дом у Невского', 59.9271165641742, 30.367916499999986, 1],
        ['Дом Четыре горизонта', 59.95758406415908, 30.40551099999995 , 1],
        ['Дом на Кирочной', 59.940789564147074, 30.38387049999997, 1],
        ['ЖК The Residence', 59.939397011662315,30.251503999999994, 1],
        ['ЖК Leningrad', 60.026662564056046, 30.3225784999999, 1],
        ['ЖК Русский дом', 59.93976656414443, 30.353183999999914, 1],
        ['ЖК One Trinity Place', 59.964376564145354, 30.277339499999975, 1],
        ['ЖК Гранвиль', 59.9341410641611, 30.27715949999995, 1],
        ['ЖК Московский', 59.90728406421673, 30.3178355 , 1],
        ['ЖК Москва', 59.842060564267, 30.308645499999972, 1],
        ['ЖК Времена года', 59.90427902954201, 30.313904999999995 , 1],
        ['ЖК ЦДС Московский', 59.814111637366025,30.341445656066814, 1],
        ['ЖК Promenade', 59.906184403086314, 30.31365882936094, 1],
        ['ЖК Legenda Комендантского', 60.03302369863507, 30.228709033729515, 1],
        ['ЖК Legenda Дальневосточного', 59.91372656420209, 30.43847899999998 , 1],
        ['ЖК Ostrov', 59.959410106941554, 30.27214562698362, 1],
        ['Квартал LIFE-Приморский', 59.98303151378077, 30.237660999999914, 1],
        ['ЖК Светлый мир Я-Романтик', 59.93787931770418,30.20034016665646, 1],
        ['ЖК Life-Лесная', 59.98335659073821,30.35609277249143, 1],
    ];

    function setMarkers(map) {
        var markers = [];
        var image = {
            url: 'img/map_marker.png',
            size: new google.maps.Size(43, 42),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(21, 42)
        };
        for (var i = 0; i < comp_objects.length; i++) {
            var comp_object = comp_objects[i];
            var marker = new google.maps.Marker({
                position: {lat: comp_object[1], lng: comp_object[2]},
                map: map,
                icon: image,
                title: comp_object[0],
                zIndex: comp_object[3],
            });
            markers.push(marker);
        }
        return markers;
    }




</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLBHOVoX3AN0xn_BQ-N8PPVarqiQYbcQk&callback=initMap">
</script>