<div class="modal_wrapper" id="modal_quiz" >
    <div class="modal_block modal_quiz">
        <div class="quiz_outer">
            <div class="btn_user_quiz_close modal_close"><div class="inner"></div></div>
            <div class="quiz_wrap">
                <form class="quiz_track send_form" name="quiz_form">

                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">1%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">1</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Где планируете <br> делать ремонт? </p>

                                        <div class="check_list check_list_repair_building">
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="building_repair" value="Новостройка" autocomplete="off" checked="checked">
                                                    <span class="icon"></span>
                                                    <span class="text">Новостройка</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="building_repair" value="Вторичное жилье"  autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Вторичное жилье</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="building_repair" value="Загородный дом"  autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Загородный дом</span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-1'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">1%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                Пожалуйста, выберите один из объектов, который хотите отремонтировать
                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">1</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">2%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">2</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Выберите количество <br>   комнат</p>

                                        <div class="check_multi_list">
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="checkbox" name="flat_studio" value="Студия" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Студия</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="count_rooms" value="1" checked="checked" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">1</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="count_rooms" value="2" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">2</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="count_rooms" value="3" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">3</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="count_rooms" value="4" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">4</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="count_rooms" value="5" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">5</span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-2'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">2%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                Если у вас двушка-студия <br>
                                                или трешка-студия, выберите <br>
                                                "студия" и подходящую цифру
                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">2</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">3%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">3</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Какая площадь квартиры?</p>

                                        <div class="quiz_ui_slider user_ui_slider">
                                            <input type="hidden" name="flat_square" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-3'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">3%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                Перемещайте курсор <br> влево-вправо, пока он <br> не остановится на нужном <br> значении.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">3</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">4%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">4</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">В каком состоянии <br> помещение?</p>

                                        <div class="premises_outer">
                                            <div class="premises_box">
                                                <div class="item">
                                                    <label class="premise_check_unit">
                                                        <input type="radio" name="state_premise" value="Голые стены" checked="checked" autocomplete="off">
                                                        <span class="cover check_img_outer">
                                                    <img src="img/premise_1.jpg" alt="">
                                                </span>
                                                        <span class="text">Голые стены</span>
                                                    </label>
                                                </div>
                                                <div class="item">
                                                    <label class="premise_check_unit">
                                                        <input type="radio" name="state_premise" value="Отделка застройщика" autocomplete="off">
                                                        <span class="cover check_img_outer">
                                                      <img src="img/premise_2.jpg" alt="">
                                                </span>
                                                        <span class="text">Отделка <br>застройщика</span>
                                                    </label>
                                                </div>
                                                <div class="item">
                                                    <label class="premise_check_unit">
                                                        <input type="radio" name="state_premise" value="Старая отделка" autocomplete="off">
                                                        <span class="cover check_img_outer">
                                                      <img src="img/premise_3.jpg" alt="">
                                                </span>
                                                        <span class="text">Старая отделка</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-4'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">4%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                Нужно понять будет ли демонтаж. <br>
                                                Если голые стены - демонтаж не нужен. Если отделка от застройщика или старая отделка во вторичке - точно нужно будет что-то демонтировать.

                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">4</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">5%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">5</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Собираетесь ли делать <br> перепланировку?</p>

                                        <div class="check_list">
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="replaning" value="Собираемся, нужна помощь с согласованием" checked="checked" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Собираемся, нужна помощь <br>с согласованием</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="replaning" value="Будем делать, помощь не нужна" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Будем делать, помощь не нужна</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="check_block _dark">
                                                    <input type="radio" name="replaning" value="Перепланировки не будет" autocomplete="off">
                                                    <span class="icon"></span>
                                                    <span class="text">Перепланировки не будет</span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-5'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">5%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                Часто даже в новой квартире жильцы хотят поменять планировку "под себя".<br>
                                                Если нужно помочь согласовать снос стен с управляющей компанией, выберите первый вариант.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">5</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">6%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">6</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Если сейчас удобно, пришлите нам план квартиры <b>(можно пропустить вопрос)</b></p>


                                        <label class="file_area  file_box">
                                            <input type="file" name="file" autocomplete="off">
                                            <span class="btn_user_file static">Выберите файл</span>
                                            <span class="text static">
                                            или <br>
                                            Перетяните его в это поле
                                        </span>
                                            <span class="text dynamic">Загружен файл:</span>
                                            <span class="text dynamic value_text"></span>
                                        </label>
                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-6'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">6%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>
                                                План квартиры нужен для составления рабочей документации. <br>
                                                Если план под рукой - прикрепите его к опросу. <br>
                                                Если долго искать - пропустите этот вопрос пока.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">6</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="quiz_slide">
                        <div class="quiz_box">
                            <div class="quiz_side_1">
                                <div class="sale_label_box _second">
                                    <div class="text">Ваша скидка</div>
                                    <div class="sale_label">
                                        <img src="img/quiz_percent.png" alt="">
                                        <div class="value">7%</div>
                                    </div>
                                </div>
                                <div class="quiz_counter _second">
                                    <div class="current">7</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                                <div class="quiz_side_content">
                                    <div class="quiz_body">
                                        <p class="quiz_title">Выберите ваш подарок!</b></p>
                                        <div class="premises_outer">
                                            <div class="premises_box">
                                                <div class="item">
                                                    <label class="premise_check_unit round">
                                                        <input type="radio" name="quiz_present" value="1 тонна штукатурки" checked="checked" autocomplete="off" data-value="0" class="input_check_quiz_present">
                                                        <span class="cover check_img_outer round">
                                                    <img src="img/present_1.jpg" alt="">
                                                </span>
                                                        <span class="text">1 тонна <br>штукатурки</span>
                                                    </label>
                                                </div>
                                                <div class="item">
                                                    <label class="premise_check_unit">
                                                        <input type="radio" name="quiz_present" value="Натяжной потолок в одной комнат" autocomplete="off" data-value="1" class="input_check_quiz_present">
                                                        <span class="cover check_img_outer round">
                                                      <img src="img/present_2.jpg" alt="">
                                                </span>
                                                        <span class="text">Натяжной <br> потолок <br> в одной комнате</span>
                                                    </label>
                                                </div>
                                                <div class="item">
                                                    <label class="premise_check_unit">
                                                        <input type="radio" name="quiz_present" value="Робот пылесос" autocomplete="off" data-value="2" class="input_check_quiz_present">
                                                        <span class="cover check_img_outer round">
                                                      <img src="img/present_3.jpg" alt="">
                                                </span>
                                                        <span class="text">Робот пылесос</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="quiz_controls_box">
                                        <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                        <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-7'); return true;"><span class="btn_text">Далее</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="quiz_side_2">
                                <div class="quiz_spot_layer">
                                    <img src="img/quiz_spot.png" alt="">
                                </div>
                                <div class="quiz_prez_content">
                                    <div class="sale_label_box _first">
                                        <div class="text">Ваша скидка</div>
                                        <div class="sale_label">
                                            <img src="img/quiz_percent.png" alt="">
                                            <div class="value">7%</div>
                                        </div>
                                    </div>

                                    <div class="quiz_master_content_box">
                                        <div class="quiz_master_unit">
                                            <div class="icon"><img src="img/popup_master.png" alt=""></div>
                                            <div class="text">
                                                <p class="name">Алексей Филютович</p>
                                                <p class="spec">
                                                    Главный строитель <br>
                                                    ДЗЕН-ДОМ
                                                </p>
                                            </div>
                                        </div>
                                        <div class="quiz_master_text">
                                            <p>Самая приятная часть опроса)</p>
                                        </div>
                                    </div>

                                    <div class="quiz_get_wrap">
                                        <div class="quiz_get_box">
										<!--
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_1.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Сертификат <br>
                                                            в “Петрович” <br>
                                                            на 5000р</p>
                                                    </div>
                                                </div>
                                            </div>
										-->
                                            <div class="item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/quiz_get_2.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Технический <br>
                                                            дизайн проект</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="quiz_counter _first">
                                    <div class="current">7</div>
                                    <div class="divider">/</div>
                                    <div class="all">7</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="quiz_slide quiz_single_bonus_slide">
                        <div class="quiz_single_bonus_box">
                            <div class="quiz_single_bonus_content">
                                <p class="quiz_title">Ваши бонусы и подарки</p>

                                <div class="quiz_get_box_wrap">
                                    <div class="quiz_get_box final_quiz_get_box">
									<!--
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_1.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Сертификат <br>
                                                        в “Петрович” <br>
                                                        на 5000р</p>
                                                </div>
                                            </div>
                                        </div>
									-->
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_3.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Скидка 7%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_2.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Технический <br>
                                                        дизайн проект</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item final_select_box_present">
                                            <div class="present_item active">
                                                <div class="quiz_get_unit">
                                                    <div class="round_quiz_get_icon">
                                                        <img src="img/present_1.jpg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>1 тонна штукатурки</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="present_item">
                                                <div class="quiz_get_unit">
                                                    <div class="round_quiz_get_icon">
                                                        <img src="img/present_2.jpg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Натяжной  потолок в одной комнате</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="present_item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/present_quize_robot.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Робот пылесос</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="quiz_controls_box">
                                <div class="quiz_btn_back quiz_prev"><img src="img/quiz_back_arrow.png" alt=""></div>
                                <div class="btn_light quiz_btn_next quiz_next" onclick="ym(55117402, 'reachGoal', 'quiz-btn-8'); return true;"><span class="btn_text">Далее</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="quiz_slide">
                        <div class="quiz_bonus_box">
                            <div class="side_1 quiz_bonus_item_1 quiz_bonus_item">
                                <p class="quiz_title">Ваши бонусы и подарки</p>
                                <div class="quiz_get_box_wrap">
                                    <div class="quiz_get_box final_quiz_get_box">
									<!--
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_1.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Сертификат <br>
                                                        в “Петрович” <br>
                                                        на 5000р</p>
                                                </div>
                                            </div>
                                        </div>
									-->
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_3.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Скидка 7%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="quiz_get_unit">
                                                <div class="icon">
                                                    <img src="img/quiz_get_2.png" alt="">
                                                </div>
                                                <div class="text">
                                                    <p>Технический <br>
                                                        дизайн проект</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item final_select_box_present">
                                            <div class="present_item active">
                                                <div class="quiz_get_unit">
                                                    <div class="round_quiz_get_icon">
                                                        <img src="img/present_1.jpg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>1 тонна штукатурки</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="present_item">
                                                <div class="quiz_get_unit">
                                                    <div class="round_quiz_get_icon">
                                                        <img src="img/present_2.jpg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Натяжной  потолок в одной комнате</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="present_item">
                                                <div class="quiz_get_unit">
                                                    <div class="icon">
                                                        <img src="img/present_quize_robot.png" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <p>Робот пылесос</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="side_2 quiz_bonus_item_2 quiz_bonus_item">
                                <p class="quiz_title_2">Куда Вам прислать <b>результаты?</b></p>
                                <div class="quiz_sun_spot_layer">
                                    <img src="img/quiz_sun_spot.jpg" alt="">
                                </div>
                                <div class="quiz_bonus_content_inner">

                                    <div class="input_item">
                                        <label class="input"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                                    </div>

                                    <div class="quiz_send_check_wrap">
                                        <div class="quiz_send_check_box">
                                            <div class="item">
                                                <label class="quiz_send_check">
                                                    <input type="radio" name="quiz_send" value="SMS" autocomplete="off" checked="checked">
                                                    <span class="icon">
<svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="47px" height="34px" viewBox="0 0 47 34">
<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
      d="M31.788,17.034 L47.000,4.009 L47.000,29.865 L31.788,17.034 ZM1.152,0.722 C1.695,0.271 2.388,-0.001 3.142,-0.001 L43.858,-0.001 C44.642,-0.001 45.360,0.295 45.912,0.780 L23.561,19.917 L1.152,0.722 ZM-0.000,29.977 L-0.000,3.896 L15.341,17.037 L-0.000,29.977 ZM23.561,24.078 L29.355,19.116 L45.990,33.148 C45.428,33.674 44.680,34.000 43.858,34.000 L3.142,34.000 C2.351,34.000 1.627,33.698 1.073,33.206 L17.773,19.120 L23.561,24.078 Z"/>
</svg>
                                            </span>
                                                    <span class="text">SMS</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="quiz_send_check">
                                                    <input type="radio" name="quiz_send" value="Viber" autocomplete="off">
                                                    <span class="icon">
<svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="46px" height="46px" viewBox="0 0 46 46">
<path fill-rule="evenodd"  fill="rgb(255, 223, 5)"
      d="M23.404,44.805 C19.477,44.805 15.744,43.804 12.510,42.032 L-0.000,46.000 L4.080,33.979 C2.039,30.590 0.846,26.622 0.846,22.383 C0.846,22.075 0.846,21.729 0.885,21.421 C1.386,9.478 11.279,-0.000 23.404,-0.000 C35.684,-0.000 45.652,9.709 45.962,21.844 C45.962,22.037 46.000,22.191 46.000,22.383 C46.000,34.789 35.876,44.805 23.404,44.805 ZM42.343,20.920 C41.574,11.212 33.374,3.545 23.404,3.545 C13.587,3.545 5.466,11.018 4.542,20.573 C4.464,21.190 4.426,21.767 4.426,22.383 C4.426,26.506 5.774,30.359 8.044,33.440 L5.696,40.452 L12.972,38.140 C15.975,40.106 19.554,41.260 23.404,41.260 C33.874,41.260 42.382,32.786 42.382,22.383 C42.382,21.921 42.382,21.421 42.343,20.920 ZM30.756,32.786 C29.755,32.863 29.755,33.595 24.096,31.399 C18.477,29.203 14.935,23.501 14.666,23.115 C14.474,22.846 13.241,21.266 12.703,19.302 C12.510,18.724 12.393,18.068 12.393,17.452 C12.393,14.717 13.818,13.406 14.358,12.867 C14.858,12.290 15.435,12.174 15.821,12.174 C16.205,12.174 16.552,12.213 16.897,12.213 C17.206,12.213 17.669,12.021 18.129,13.137 C18.593,14.216 19.707,16.951 19.825,17.221 C19.978,17.490 20.055,17.800 19.862,18.185 C19.746,18.416 19.670,18.608 19.515,18.839 C19.477,18.916 19.400,18.993 19.323,19.070 C19.054,19.417 18.746,19.803 18.477,20.034 C18.206,20.342 17.937,20.612 18.245,21.190 C18.554,21.729 19.670,23.501 21.325,24.965 C23.442,26.853 25.212,27.431 25.790,27.701 C26.329,27.971 26.636,27.932 26.984,27.546 C27.292,27.201 28.369,25.967 28.717,25.428 C29.101,24.849 29.447,24.965 29.987,25.119 C30.487,25.312 33.182,26.660 33.758,26.930 C34.298,27.201 34.683,27.354 34.799,27.546 C34.952,27.776 34.952,28.894 34.491,30.167 C34.028,31.476 31.757,32.708 30.756,32.786 Z"/>
</svg>
                                            </span>
                                                    <span class="text">Viber</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="quiz_send_check">
                                                    <input type="radio" name="quiz_send" value="WhatsApp" autocomplete="off">
                                                    <span class="icon">
<svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="48px" height="50px" viewBox="0 0 48 50">
<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
      d="M46.916,31.880 L46.901,31.937 C45.744,36.656 40.523,41.718 35.729,42.772 L35.675,42.783 C31.799,43.528 27.894,43.901 23.991,43.901 C22.842,43.901 21.693,43.864 20.545,43.800 L15.250,49.348 C13.940,50.724 11.635,49.788 11.635,47.881 L11.635,42.615 C7.020,41.285 2.188,36.454 1.078,31.936 L1.066,31.880 C-0.366,25.290 -0.366,18.606 1.066,12.016 L1.078,11.959 C2.238,7.239 7.458,2.177 12.250,1.123 L12.307,1.112 C20.059,-0.379 27.921,-0.379 35.675,1.112 L35.729,1.123 C40.523,2.177 45.744,7.239 46.901,11.959 L46.916,12.016 C48.347,18.606 48.347,25.290 46.916,31.880 ZM42.727,12.973 C41.953,9.894 37.991,6.066 34.840,5.360 C27.641,3.978 20.340,3.978 13.139,5.360 C9.990,6.066 6.028,9.894 5.254,12.973 C3.965,18.929 3.965,24.965 5.254,30.922 C6.028,34.001 9.990,37.829 13.139,38.536 C13.140,38.536 13.141,38.536 13.142,38.536 C13.282,38.565 13.385,38.687 13.385,38.830 L13.385,47.557 C13.385,47.993 13.913,48.208 14.213,47.893 L18.325,43.639 C18.325,43.639 21.601,40.247 22.191,39.639 C22.248,39.578 22.327,39.546 22.410,39.549 C26.562,39.664 30.718,39.326 34.840,38.536 C37.990,37.830 41.953,34.001 42.727,30.923 C44.016,24.965 44.016,18.929 42.727,12.973 ZM35.263,23.201 C34.797,23.206 34.587,22.814 34.557,22.395 C34.498,21.562 34.455,20.723 34.341,19.900 C33.740,15.536 30.291,11.928 25.984,11.155 C25.337,11.037 24.673,11.007 24.017,10.938 C23.601,10.893 23.058,10.869 22.967,10.348 C22.889,9.912 23.254,9.566 23.666,9.543 C23.778,9.537 23.890,9.542 24.002,9.542 C29.810,9.707 34.581,13.592 35.603,19.380 C35.777,20.366 35.840,21.375 35.917,22.375 C35.950,22.797 35.713,23.197 35.263,23.201 ZM24.002,9.542 C29.810,9.707 23.890,9.542 24.002,9.542 ZM24.023,13.292 C24.042,12.918 24.313,12.647 24.719,12.672 C26.051,12.747 27.339,13.037 28.524,13.670 C30.935,14.956 32.310,16.987 32.713,19.698 C32.730,19.821 32.760,19.941 32.769,20.066 C32.791,20.369 32.806,20.676 32.830,21.077 C32.819,21.149 32.814,21.322 32.772,21.483 C32.618,22.071 31.735,22.146 31.531,21.551 C31.470,21.376 31.460,21.176 31.460,20.986 C31.458,19.745 31.191,18.506 30.571,17.428 C29.933,16.318 28.959,15.387 27.817,14.824 C27.125,14.483 26.378,14.269 25.621,14.144 C25.291,14.089 24.956,14.055 24.623,14.009 C24.220,13.952 24.005,13.692 24.023,13.292 ZM28.421,20.223 C28.386,19.910 28.358,19.592 28.284,19.288 C28.138,18.689 27.821,18.132 27.322,17.765 C27.085,17.590 26.817,17.465 26.536,17.383 C26.179,17.280 25.810,17.308 25.454,17.221 C25.067,17.125 24.855,16.808 24.915,16.444 C24.969,16.112 25.292,15.853 25.650,15.878 C27.904,16.043 29.513,17.217 29.742,19.887 C29.758,20.076 29.778,20.275 29.735,20.456 C29.666,20.757 29.447,20.907 29.231,20.930 C29.267,20.930 29.278,20.931 29.218,20.935 C28.918,20.940 29.071,20.933 29.182,20.930 C28.721,20.923 28.470,20.660 28.421,20.223 ZM24.447,27.849 C24.958,28.065 25.471,28.314 26.061,28.244 C27.047,28.128 27.367,27.039 28.059,26.467 C28.735,25.911 29.600,25.902 30.328,26.367 C31.055,26.832 31.761,27.331 32.463,27.835 C33.152,28.329 33.837,28.813 34.473,29.379 C35.084,29.921 35.295,30.631 34.950,31.367 C34.320,32.716 33.404,33.837 32.082,34.554 C31.709,34.755 31.263,34.820 30.842,34.952 C30.357,34.804 29.897,34.704 29.468,34.524 C25.027,32.667 20.940,30.272 17.704,26.599 C15.863,24.510 14.422,22.154 13.205,19.659 C12.627,18.473 12.140,17.244 11.644,16.022 C11.191,14.908 11.858,13.759 12.559,12.920 C13.217,12.132 14.064,11.528 14.982,11.084 C15.698,10.737 16.405,10.937 16.927,11.550 C18.058,12.873 19.097,14.262 19.938,15.797 C20.455,16.739 20.313,17.890 19.376,18.534 C19.148,18.690 18.940,18.873 18.729,19.048 C18.542,19.202 18.367,19.358 18.240,19.569 C18.006,19.952 17.995,20.402 18.146,20.817 C19.301,24.019 21.249,26.510 24.447,27.849 Z"/>
</svg>
                                            </span>
                                                    <span class="text">WhatsApp</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label class="quiz_send_check">
                                                    <input type="radio" name="quiz_send" value="Telegram" autocomplete="off">
                                                    <span class="icon">
<svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="38px" height="36px" viewBox="0 0 38 36">
<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
      d="M35.558,0.142 L0.816,15.369 C-0.309,15.862 -0.241,17.678 0.917,18.063 L9.745,20.995 L13.041,32.753 C13.385,33.981 14.768,34.354 15.570,33.434 L20.134,28.192 L29.090,35.587 C30.186,36.491 31.747,35.819 32.025,34.323 L37.957,2.426 C38.247,0.864 36.881,-0.438 35.558,0.142 ZM31.203,7.221 L15.065,23.271 C14.904,23.431 14.802,23.652 14.778,23.892 L14.156,30.104 C14.136,30.307 13.885,30.333 13.830,30.139 L11.274,20.874 C11.157,20.450 11.309,19.990 11.645,19.756 L30.702,6.463 C31.140,6.157 31.589,6.837 31.203,7.221 Z"/>
</svg>
                                            </span>
                                                    <span class="text">Telegram</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn_wrap">
                                        <button type="submit"  class="btn_light btn _yellow btn_quiz_submit" onclick="ym(55117402, 'reachGoal', 'quiz-btn-9'); return true;">
                                            <span class="btn_text">Получить расчет и подарки</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="quiz_slide quiz_slide_end">
                        <div class="quiz_slide_final">
                            <div class="quiz_spot_final_layer">
                                <img src="img/quiz_spot_final.png" alt="">
                            </div>
                            <div class="quiz_lazy_man_layer">
                                <img src="img/lazy_man_1.png" alt="" class="_desktop">
                                <img src="img/lazy_man_1_mobile.png" alt="" class="_mobile">
                            </div>

                            <p class="page_title">
                                Прекрасно! Сметный отдел <br>
                                уже <b>занимается расчетом</b>
                            </p>
                            <div class="quiz_slide_final_inner">
                                <div class="text">
                                    <p>Можно закрыть сайты конкурентов <br> и помечтать о том, как вскоре преобразится <br> ваше жилье.</p>
                                    <p>Вы уже видели наши аккаунты в соцсетях? <br>
                                        Стараемся писать только с пользой и по делу… <br>
                                        почти всегда :)</p>
                                </div>

                                <div class="socials_modal_box">
                                    <div class="item">
                                        <a href="https://vk.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/vk_icon.svg" alt=""></a>
                                    </div>
                                    <div class="item">
                                        <a href="https://www.instagram.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/instagram_icon.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="socials_modal_box _mobile">
                                <div class="item">
                                    <a href="https://vk.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/vk_icon.svg" alt=""></a>
                                </div>
                                <div class="item">
                                    <a href="https://www.instagram.com/dzen_dom" class="social_big_unit" target="_blank"><img src="img/instagram_icon.svg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="from_where" class="input_where" value="Модальное окно: Квиз(quiz)">
                </form>
            </div>
        </div>
    </div>
</div>

