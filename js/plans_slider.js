"use strict";

$(document).ready(function(){
    var plans_slider_wrap = $('.plans_slider_wrap');
    if (plans_slider_wrap.length) {
        plans_slider_wrap.each( function() {
            var current_slide_count, slides_all_count, slider, slides, slider_wrap, slider_btn_next, slider_btn_prev, plan_view_wrap;
            slider_wrap = $(this);
            current_slide_count = slider_wrap.find('.current_slide_num').eq(0);
            slides_all_count = slider_wrap.find('.all_slides_count').eq(0);
            slider_btn_next = slider_wrap.find('.btn_slider._next').eq(0);
            slider_btn_prev = slider_wrap.find('.btn_slider._prev').eq(0);
            slider = slider_wrap.find('.plans_slider').eq(0);
            slides = slider.find('.plans_slide_item');
            plan_view_wrap = slider_wrap.find('.plan_view_wrap');

            /*processing*/
            slides.eq(0).addClass('slide_active');
            slides_all_count.html(format_nums(slides.length));
            current_slide_count.html(format_nums(1));

            hide_btn_slider(slides, slider_btn_prev, slider_btn_next);

            slider_btn_prev.on('click', function() {
                do_slide(0, slides, current_slide_count);
                hide_btn_slider(slides, slider_btn_prev, slider_btn_next);
                plan_view_wrap.removeClass('view_active');
            });

            slider_btn_next.on('click', function() {
                do_slide(1, slides, current_slide_count);
                hide_btn_slider(slides, slider_btn_prev, slider_btn_next);
                plan_view_wrap.removeClass('view_active');
            });

            }
        )
    }

});

function do_slide(a, slides, dom_current_val) {
    var current_slide_index = slides.filter('.slide_active').eq(0).index();

    /*prev*/
    if (a === 0) {
        if (current_slide_index !== 0) {
            slides.removeClass('slide_active');
            slides.eq(--current_slide_index).addClass('slide_active');
        }
    }
    /*next*/
    if (a === 1) {
        if (current_slide_index < slides.length - 1) {
            slides.removeClass('slide_active');
            slides.eq(++current_slide_index).addClass('slide_active');
        }
    }
    dom_current_val.html(format_nums(slides.filter('.slide_active').eq(0).index() + 1));

}


function hide_btn_slider(slides, btn_prev, btn_next) {
    var a = check_edge_slides(slides);
    if (a === 0) {
        btn_next.removeClass('btn_disabled');
        btn_prev.addClass('btn_disabled');
    } else if (a === 1) {
        btn_prev.removeClass('btn_disabled');
        btn_next.addClass('btn_disabled');
    } else {
        btn_prev.add(btn_next).removeClass('btn_disabled');
    }

    function check_edge_slides(slides) {
        if (slides.eq(0).hasClass('slide_active')) {
            return 0;
        }
        if (slides.eq(slides.length-1).hasClass('slide_active')) {
            return 1;
        }
        return -1;
    }

}


function format_nums(a) {
    a = parseInt(a, 10);
    if (a < 9) {
        a = '0' + a;
    }
    return a;
}
