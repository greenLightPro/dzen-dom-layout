$(document).ready(function(){

    var user_ui_slider = $('.user_ui_slider');
    if(user_ui_slider.length) {
        user_ui_slider.each(function(){
            var slider  = $(this);
            var slider_input = slider.find('input');

            slider.slider({
                min: 20,
                max:200,
                step: 1,
                value: 40,
                create: function() {
                    var $this_value = $(this).slider("value");
                    slider_input.val( $this_value);
                    slider.find('.ui-slider-handle').html('<i class="ui_value"></i>');
                    slider.find('.ui_value').html($this_value + 'м<sup>2</sup>');
                },
                slide: function( event, ui ) {
                    var $this_value = $(this).slider("value");
                    slider.find('.ui_value').html($this_value + 'м<sup>2</sup>');
                },
                change: function( event, ui ) {
                    var $this_value = $(this).slider("value");
                    slider_input.val( $this_value);
                    slider.find('.ui_value').html($this_value + 'м<sup>2</sup>');
                },

            });

        });
    }

    $(document).on('change', '.file_box input', function () {
        var $this, file_value, path_array, file_name;
        $this = $(this);
        file_value = $this.val();
        path_array = file_value.split('\\');
        file_name = (path_array[path_array.length-1]);
        if (file_value.length > 0 && file_value.length != '') {
            $this.closest('.file_box').find('.value_text').html(file_name);
            $this.closest('.file_box').addClass('active');
        } else {
            $this.closest('.file_box').find('.value_text').html('');
            $this.closest('.file_box').removeClass('active');
        }
    });

    var quiz_outer = $('.quiz_outer');
    if(quiz_outer.length) {
        quiz_outer.each(function(){
            var quiz_outer = $(this);
            var quiz_track = quiz_outer.find('.quiz_track');
            var final_select_box_present = quiz_track.find('.final_select_box_present');
            quiz_outer.find('.quiz_next').on('click', function(){
                var i = $(this).closest('.quiz_slide').index();
                quiz_track.css({
                    '-webkit-transform': 'translateX(-'+ 100 * (i+1) + '%)',
                    'transform': 'translateX(-'+ 100 * (i+1) + '%)',
                });
                $('#modal_quiz').scrollTop(0);
            });
            quiz_outer.find('.quiz_prev').on('click', function(){
                var i = $(this).closest('.quiz_slide').index();
                quiz_track.css({
                    '-webkit-transform': 'translateX(-'+ 100 * (i-1) + '%)',
                    'transform': 'translateX(-'+ 100 * (i-1) + '%)',
                });
                $('#modal_quiz').scrollTop(0);
            });
            quiz_outer.find('.btn_user_quiz_close').on('click', function(){
                quiz_track.css({
                    '-webkit-transform': 'translateX(0%)',
                    'transform': 'translateX(0%)',
                });
                quiz_outer.find('form').trigger('reset');
                final_select_box_present.each(function(){
                    var present_items = $(this).find('.present_item');
                    present_items.removeClass('active');
                    present_items.eq(0).addClass('active');
                });

                $('#modal_quiz').scrollTop(0);
            });
            quiz_outer.find('.input_check_quiz_present').on('change', function(){
                var inp_val = parseInt($(this).data('value'));
                  final_select_box_present.each(function(){
                    var present_items = $(this).find('.present_item');
                    present_items.removeClass('active');
                    present_items.eq(inp_val).addClass('active');
                });


            });

        });
    }

    $(document).on('click', '.btn_start_modal_quiz', function () {
        var i  = parseInt($(this).closest('.start_quiz_form').find('.check_block input:checked').val(), 10);
        var modal_quiz = $('#modal_quiz');
        modal_quiz.find('.quiz_side_1 .check_list_repair_building > .item').eq(i).find('input').prop('checked', true);
        modal_quiz.find('.quiz_track').css({
            '-webkit-transform': 'translateX(-100%)',
            'transform': 'translateX(-100%)'
        });

    })


});