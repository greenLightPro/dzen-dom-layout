jQuery(document).ready(function(){

    var scrollbar_wrap = $('.scrollbar_wrap');
    if (scrollbar_wrap.length) {
        scrollbar_wrap.scrollbar({
            'autoScrollSize' : true,
            'ignoreMobile' : false,
            'ignoreOverlay' : true,
        });
    }

    $(document).on('click', '.btn_view', function(){
        $(this).closest('.view_wrap').toggleClass('view_active')
    });

    var team_slider = $('.team_slider');
    if(team_slider.length) {
        team_slider.each(function(){
            var $slider, $wrap;
            $slider = $(this);
            $wrap = $slider.closest('.slider_wrap');
            $slider.slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                lazyLoad: 'ondemand',
                prevArrow: $wrap.find('.btn_slider.prev'),
                nextArrow: $wrap.find('.btn_slider.next'),
                responsive: [
                    {
                        breakpoint: 961,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 701,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 461,
                        settings: {
                            slidesToShow: 1
                        }
                    },
                ]
            });
        });
    }

    $(document).on('click', '.clock_num_unit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var wrap = $this.closest('.process_clock_box');
        wrap.find('.clock_tips > .clock_tip_unit').eq($this.index()).addClass('active');
        wrap.addClass('active');
    });
    $(document).on('click', '.clock_close', function (e) {
        var $this = $(this);
        var wrap = $this.closest('.process_clock_box');
        wrap.find('.clock_tips > .clock_tip_unit').removeClass('active');
        wrap.removeClass('active');
    });


    sticking('#fluid_header_block', 700, '#super_digital_block');
    $(window).on('scroll resize', function () {
        sticking('#fluid_header_block', 700, '#super_digital_block');
    });


    var lg_box, light_gallery_settings;

    light_gallery_settings = {
        thumbnail:true,
        animateThumb: false,
        showThumbByDefault: false,
        selector: '.lg_item',
        download: false,
        youtubePlayerParams: {
            modestbranding: 1,
            showinfo: 0,
            rel: 0,
            controls: 1
        }
    };

    /*
    lg_box  = $('.lg_box');
    if(lg_box.length) {
        lg_box.lightGallery(light_gallery_settings);
    }

     */

    var input_phone = $('input[type="tel"]');
    if(input_phone.length) {
        input_phone.mask('+7(000) 000-00-00');
        input_phone.on('focus', function () {
            if ($(this).val().length === 0) {
                $(this).val('+7 (');
            }
        });
    }
    $(document).on('focus change', 'input, textarea', function(){
        $(this).removeClass('invalid');
    });
    $(document).on('blur', 'input[type=tel]', function(){
        var $this = $(this);
        if (  $this.val().length !== 17) {
            $this.val('');
        }
    });


    $(".send_form").submit(function () {
        var $this, form_data;
        $this = $(this);
        form_data = new FormData($this[0]);
		var city_name = $('body').attr('data-city');
		if (city_name) {
			city_name = city_name.trim();
			form_data.append('city', city_name);
		}
		form_data
        if (is_empty($this)) {
            $.ajax({
                type: "POST",
                url: "php/mail.php",
                data: form_data,
                contentType:false,
                processData:false,
                success: function (res) {
                    $this.trigger('reset');
                    var file_box = $this.find('.file_box');
                    if(file_box.length) {
                        file_box.removeClass('active');
                        file_box.find('.value_text').html('');
                    }

                    if ($this.attr('name') === 'quiz_form') {
                        var  modal_quiz = $this.closest('.modal_quiz');
                        var slide_len = modal_quiz.find('.quiz_slide').length - 1 ;
                        if ($(window).width() > 760) {
                            slide_len = slide_len - 1;
                        }
                        modal_quiz.scrollTop(0);
                        modal_quiz.find('.quiz_track')
                            .css({
                            '-webkit-transform': 'translateX('+ -100 * slide_len +'%)',
                            'transform': 'translateX('+ -100 * slide_len +'%)'
                        });
                    } else {
                        modal_to_close('.modal_wrapper');
                        center_modal('#modal_send');
                    }
                    document.cookie = "user_send=1; expires="+ last_time() +"; path=/; ";
                }
            });
        }
        return false;
    });

    lg_box = $('.lg_box');
    if(lg_box.length) {
        lg_box.lightGallery({
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            selector: '*:not(.slick-clone) .lg_item',
            download: false,
            youtubePlayerParams: {
                modestbranding: 1,
                showinfo: 0,
                rel: 0,
                controls: 1
            }
        });
    };

    var wow = new WOW(
        {
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 150,
            mobile: false,
            live: true
        }
    );
    wow.init();

    $(document).on('click', '.btn_get_plan', function() {
        var $this = $(this);
        var obj = $($this.attr('data-id'));
        var content = $this.closest('.plans_slide_outer ').find('.plan_page_modal_content').html();
        obj.find('.plan_modal_content').html(content);
    });

});

/*lead magnit*/
var mouse_Y = 0, lead_flag = 0;
$(document).bind('mousemove', function(e) {
    mouse_Y = e.pageY;
});
$(document).on('mouseleave', function(){
    if(mouse_Y <= 100) {
        lead_func ();
    }
});

/*
var tease_time_out = setTimeout(function(){
    lead_func ();
    clearTimeout(tease_time_out);
}, 40000 );
*/



function sticking(selector, min_screen, anchor){
    var obj, win_top, stick_top;
    min_screen = min_screen || 0;
    obj = $(selector).eq(0);
    if(obj.length === 0) {return 0;}
    if(anchor === 'window'){
        stick_top = $(window).height();
    } else if ((typeof anchor) === 'string' && $(anchor).length ) {
        stick_top = $(anchor).offset().top;

    } else if(typeof anchor === 'number') {
        stick_top = anchor;
    } else {
        stick_top = 0;
    }
    if($(window).width() <= min_screen){
        obj.removeClass('stick');
        return 0;
    }
    win_top = $(window).scrollTop();
    if(win_top >= stick_top){
        obj.addClass('stick');
    } else {
        obj.removeClass('stick');
    }
}

function has_attr (attr) {
    return (typeof attr !== typeof undefined && attr !== false);
}

function is_empty(elem) {
    var mas, objects;
    objects = elem.find('.req');
    mas = [];
    objects.each(function () {
        var $this = $(this);
        if ($this.val().length === 0 || !$this.val().replace(/\s+/g, '')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if (($this.attr('type') === "checkbox") && !$this.prop('checked')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if ($this.attr('type')=== "tel" && $this.val().length !== 17) {
            $this.addClass('invalid');
            mas.push("0");
        }
        else{
            $this.removeClass('invalid');
        }
    });
    if (mas.length === 0) return 1;
    else return 0;
}

function lead_func () {
    if (lead_flag || $('html.show_modal').length > 0) return 0;
    if(getCookie('user_send') != 1) {
        modal_overlay.fadeOut(200);
        center_modal("#modal_get_bonus");
        lead_flag = 1;
    }
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function last_time() {
    var date, date2;
    date = new Date();
    date2 = new Date (date.getFullYear(), date.getMonth(), date.getDate()+1, 0,0,0);
    date2 = date2.toDateString();
    return (date2);
}



$(document).ready(function(){
    // var area_layer = $('.area_layer');
    // if (area_layer.length) {
    //     $('.area_layer').each(function(){
    //        var area_children =  $(this).children();
    //        // for(var i = 0; i < area_children.length; i++) {
    //        //     area_children.eq(i).attr('data-index', 'index-'+i);
    //        // }
    //         area_children.click(function(){
    //             console.log($(this).attr('data-index'));
    //         })
    //
    //     });
    // }

    $('.media_plan_box').on('click', '.plan_obj_unit', function(){
        var data_gallery = $(this).attr('data-gallery');
        var obj_gallery = $(this).closest('.media_plan_box').find(data_gallery);
        console.log(obj_gallery);
        if(obj_gallery.length) {
            obj_gallery.find('.lg_item').eq(0).trigger('click');
        }
    });

});


















