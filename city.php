<?php header("Content-Type: text/html; charset=utf-8");?>
<?php 
global $city;
/*
if (!$city) {
	$city = array (
		'phone' => '+7 (846) 215-1819',	
	);}
	*/
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include "parts/header.php";?>
</head>
<body data-city="<?php echo $city['name_city']; ?>">
<div class="main_wrap">
    <div class="main_body">
        <div class="main_block_1">
            <header class="header_block">
                <div class="container">
                    <div class="header">
                        <div class="head_logo">
                            <a href="#" class="logo">
                                <span class="logo_icon"><img src="img/logo.png" alt=""></span>
                                <span class="logo_description">
                                <b>Узнайте стоимость ремонта <br>
                                квартиры,</b> получите бонусы <br>
                                и подарки
                            </span>
                            </a>
                        </div>
                        <div class="head_content">
                            <a href="#" class="btn_simple _purple btn btn_get_modal" data-id="#modal_quiz"><span class="btn_text">рассчитать стоимость</span></a>
                        </div>
                        <div class="head_contacts">
                            <p class="head_cnt_phone"><a href="tel:<?php echo preg_replace("/[^0-9\+]/",'', $city['phone']); ?>"><?php echo $city['phone']; ?></a></p>
                            <a href="#" class="btn_simple _purple btn btn_get_modal" data-id="#modal_quiz"><span class="btn_text">рассчитать стоимость</span></a>
                        </div>
                    </div>
                </div>
            </header>


            <div class="fluid_header_block" id="fluid_header_block">
                <div class="container">
                    <div class="fluid_header">
                        <div class="fluid_header_logo">
                            <p><b>Узнайте стоимость ремонта
                                    квартиры,</b><br> получите&nbsp;бонусы&nbsp;и&nbsp;подарки</p>
                        </div>
                        <div class="fluid_header_content">
                            <a href="#" class="btn_simple _purple btn btn_get_modal" data-id="#modal_quiz"><span class="btn_text">рассчитать стоимость</span></a>
                        </div>
                        <div class="fluid_head_contacts">
                            <p class="phone"><a href="tel:<?php echo preg_replace("/[^0-9\+]/",'', $city['phone']); ?>"><?php echo $city['phone']; ?></a></p>
                        </div>
                    </div>
                </div>
            </div>
            <section class="block_1_block">
                <div class="container">
                    <div class="block_1">
                        <div class="block_1_title z_layer  wow fadeInLeft">
                            <h1 class="title">РЕМОНТ КВАРТИР <br>
                                БЕЗ НЕРВОВ, <br>
                                <b>БЕЗ ПРЕДОПЛАТ <br>
                                    И ЗАДЕРЖЕК</b>
                            </h1>
                            <p class="vertical_text ">
                                Экспертно · Надежно · Выгодно</p>
                        </div>

                        <div class="block_1_preferences">
                            <div class="block_1_pref_box">
                                <div class="item  wow fadeInUp">
                                    <p>
                                        <b>1 день <br>
                                            задержки:</b>
                                    </p>
                                    <p>вычитаем 3000р</p>
                                </div>
                                <div class="item wow fadeInUp" data-wow-delay="0.15s">
                                    <p>
                                        <b>14 дней <br>
                                            задержки:</b>
                                    </p>
                                    <p>весь этап бесплатно</p>
                                </div>
                                <div class="item pt  wow fadeInUp" data-wow-delay="0.3s">
                                    <p><b>·&nbsp;согласны?&nbsp;·</b></p>
                                </div>
                                <div class="item  wow fadeInUp" data-wow-delay="0.45s">
                                    <a href="#" class="btn_light btn_get_modal" data-id="#modal_quiz"><span class="btn_text">Да! Сколько стоит?</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="img_layer"><img src="img/master.png" alt="" class="wow fadeInRight"></div>
                    </div>
                </div>
            </section>
        </div>

        <section class="super_digital_block" id="super_digital_block">
            <div class="container">
                <div class="super_digital">
                    <div class="super_digital_wrap">
                        <div class="super_digital_item _item_1 ">
                            <div class="super_digital_unit wow fadeInLeft">
                                <p class="title">2 года</p>
                                <p>
                                    <b>гарантия</b>
                                    на работы
                                </p>
                            </div>
                        </div>
                        <div class="super_digital_item _item_2">
                            <div class="super_digital_unit_big  wow fadeInLeft" data-wow-delay=".15s">
                                <div class="num">
                                    8
                                </div>
                                <div class="content">
                                    <p class="title">лет <br> опыта</p>
                                    <p>
                                        и безупречная <br>
                                        репутация
                                    </p>
                                </div>

                            </div>
                            <div class="decor_layer">
                                <img src="img/round_brush_stroke_blue.png" alt="">
                                <img src="img/round_brush_stroke_blue_vertical.png" alt="">
                            </div>
                        </div>
                        <div class="super_digital_item _item_3">
                            <div class="super_digital_box  wow fadeInLeft"  data-wow-delay=".15s">
                                <div class="item _mobile_hide">
                                    <div class="super_digital_unit">
                                        <p class="title">47</p>
                                        <p>
                                            <b>сотрудников</b>
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="super_digital_unit">
                                        <p class="title">от 2897</p>
                                        <p>
                                            <b>рублей</b> за кв.м.
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="super_digital_unit">
                                        <p class="title">860</p>
                                        <p>
                                            <b>квартир</b> сдали
                                        </p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="super_digital_unit">
                                        <p class="title">3 месяца</p>
                                        <p>
                                            <b>на ремонт</b> 2 кв.м.
                                        </p>
                                    </div>
                                </div>
                                <div class="item _added _first">
                                    <div class="super_digital_unit">
                                        <p class="title">2 года</p>
                                        <p>
                                            <b>гарантия</b>
                                            на работы
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page_quiz_block">
            <div class="container">
                <div class="page_quiz">
                    <div class="page_quiz_item  _item_1">
                        <p class="page_title wow fadeInLeft">Пройдите <b>минутный опрос</b> <br>
                            для оценки стоимости <br>
                            ремонта
                        </p>
                        <p class="subtitle wow fadeInLeft">После опроса вы получите:</p>
                        <div class="quiz_gift_wrap">
                            <div class="quiz_gift_box">
							<!--
                                <div class="item">
                                    <div class="quiz_gift_unit wow fadeInLeft">
                                        <div class="icon">
                                            <img src="img/get_icon_1.png" alt="" class="_img_1">
                                        </div>
                                        <div class="text">
                                            <p>Сертификат <br>
                                                в “Петрович” <br>
                                                на 5000р</p>
                                        </div>
                                    </div>
                                </div>
							-->
                                <div class="item">
                                    <div class="quiz_gift_unit wow fadeInLeft" data-wow-delay="0.15s">
                                        <div class="icon">
                                            <img src="img/get_icon_2.png" alt="" class="_img_2">
                                        </div>
                                        <div class="text">
                                            <p>Скидку <br>
                                                на работы <br>
                                                7%</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="quiz_gift_unit wow fadeInLeft" data-wow-delay="0.3s">
                                        <div class="icon">
                                            <img src="img/get_icon_3.png" alt="" class="_img_3">
                                        </div>
                                        <div class="text">
                                            <p>Разработку <br>
                                                технического <br>
                                                проекта</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="quiz_gift_unit wow fadeInLeft" data-wow-delay="0.45s">
                                        <div class="icon">
                                            <img src="img/get_icon_4.png" alt="" class="_img_4">
                                        </div>
                                        <div class="text">
                                            <p>Один <b>из трёх <br>
                                                    подарков</b> <br>
                                                на выбор</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn_wrap wow fadeInUp">
                            <a href="javascript:void(0)" class="btn_light btn btn_get_modal" data-id="#modal_quiz">
                                <span class="icon"></span>
                                <span class="btn_text">Узнать стоимость</span>
                            </a>
                        </div>
                    </div>
                    <div class="page_quiz_item _item_2">
                        <div class="user_form page_quiz_form start_quiz_form">
                            <p class="title wow fadeInRight">Где планируете <br>
                                делать ремонт?
                            </p>
                            <div class="check_list wow fadeInRight">
                                <div class="item">
                                    <label class="check_block">
                                        <input type="radio" name="building_repair" value="0" autocomplete="off" checked="checked">
                                        <span class="icon"></span>
                                        <span class="text">Новостройка</span>
                                    </label>
                                </div>
                                <div class="item">
                                    <label class="check_block">
                                        <input type="radio" name="building_repair" value="1" autocomplete="off">
                                        <span class="icon"></span>
                                        <span class="text">Вторичное жилье</span>
                                    </label>
                                </div>
                                <div class="item">
                                    <label class="check_block">
                                        <input type="radio" name="building_repair" value="2" autocomplete="off">
                                        <span class="icon"></span>
                                        <span class="text">Загородный дом</span>
                                    </label>
                                </div>
                            </div>

                            <div class="btn_wrap wow fadeInRight">
                                <a href="javascript:void(0)" class="btn_light btn btn_start_modal_quiz btn_get_modal" data-id="#modal_quiz">
                                    <span class="icon"></span>
                                    <span class="btn_text">Далее</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="life_problems_block">
            <div class="container">
                <div class="life_problems">
                    <div class="life_problems_item _item_1">
                        <p class="page_title wow fadeInUp">Знакомьтесь – это Саша <br>
                            и Алина. <b>Они планируют <br>
                            ремонт в квартире.</b></p>

                        <div class="problems_life_img_content">
                            <img src="img/spb_bg_part.jpg" alt="" class="_bg_desktop">
                            <img src="img/spb_bg_mobile.jpg" alt="" class="_bg_mobile">
                            <img src="img/couple_mobile.png" alt="" class="_family">
                        </div>


                        <div class="life_problems_list_wrap scrollbar-inner scrollbar_wrap scrollbar_sun">
                            <div class="life_problems_list scrollbar_inner_box wow fadeInUp">
                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">01</div>
                                        <div class="text">
                                            <p>Определиться с бюджетом <br>
                                                и реальными сроками <br>
                                                ремонта.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">02</div>
                                        <div class="text">
                                            <p>
                                                Найти строителей, которые <br>
                                                уложатся в бюджет и срок.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">03</div>
                                        <div class="text">
                                            <p>
                                                Выбрать хорошего <br>
                                                и надежного прораба.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">04</div>
                                        <div class="text">
                                            <p>
                                                Договориться сразу, что во время <br>
                                                ремонта квартира не должна <br>
                                                превращаться в свалку мусора.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">05</div>
                                        <div class="text">
                                            <p>Загуглить, где дешевле <br> купить стройматериалы.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">06</div>
                                        <div class="text">
                                            <p>Работы принимать с технадзором <br> (найти своего человека!)</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">07</div>
                                        <div class="text">
                                            <p>Никаких предоплат!!! <br> Платить только после приемки!</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="life_problems_list_unit">
                                        <div class="num">08</div>
                                        <div class="text">
                                            <p>Перед финалом проверить, <br> что мусор вывезен, проведен клининг, <br> всё работает, сантехника сверкает.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        
                    </div>
                    <div class="life_problems_item _item_2">
                        <div class="problems_life_img_big wow fadeInRight">
                            <img src="img/spb_bg_part.jpg" alt="" class="_bg">
                            <img src="img/couple.png" alt="" class="_family">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page_troubles_block">
            <div class="container">
                <div class="page_troubles">
                    <p class="page_title wow fadeInUp">
                        Выбрав из множества компаний <b>«ту самую»,</b> <br>
                        они погружаются во все тяжкие <br>
                        <b>строительной реальности</b>
                    </p>

                    <div class="page_troubles_box">
                        <div class="page_troubles_item _item_1 ">
                            <div class="page_troubles_list">

                                <div class="item wow fadeInLeft">
                                    <div class="message_unit message_corner right_corner">
                                        <div class="layer layer_1"></div>
                                        <div class="layer layer_2"></div>
                                        <div class="layer layer_3 message_content">
                                            <div class="text">
                                                <p>Мам, это кошмар! Общая стоимость уже <br> выросла на 20%, сроки растягиваются.
                                                    <br>
                                                    Угрозы санкций по договору не помогает, <br> штрафов они не боятся.
                                                </p>
                                            </div>
                                            <div class="message_options">
                                                <p>14.22</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item wow fadeInLeft" data-wow-delay=".15s">
                                    <div class="message_unit">
                                        <div class="layer layer_1"></div>
                                        <div class="layer layer_2"></div>
                                        <div class="layer layer_3 message_content">
                                            <div class="text">
                                                <p>
                                                    Рабочие все делают только как им удобно. <br>
                                                    Нас вообще не слышат. <br>
                                                    Сашка много работает, общаться с ними <br>
                                                    приходится мне <img src="img/emoji_1.png" alt="">
                                                </p>
                                            </div>
                                            <div class="message_options">
                                                <p>18.30</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item wow fadeInLeft"  data-wow-delay=".3s">
                                    <div class="message_unit">
                                        <div class="layer layer_1"></div>
                                        <div class="layer layer_2"></div>
                                        <div class="layer layer_3 message_content">
                                            <div class="text">
                                                <p>Я уже не верю, что это когда-нибудь <br>
                                                    закончится. Хочется все бросить <img src="img/emoji_2.png" alt="">
                                                </p>
                                            </div>
                                            <div class="message_options">
                                                <p>22.10</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="troubles_decor_elem _decor_elem_1">
                                <img src="img/troubles_elem_1.png" alt="">
                            </div>
                        </div>
                        <div class="page_troubles_item _item_2 ">
                            <div class="page_troubles_list">

                                <div class="item wow fadeInRight" >
                                    <div class="message_unit left_corner message_corner reverse">
                                        <div class="layer layer_1"></div>
                                        <div class="layer layer_2"></div>
                                        <div class="layer layer_3 message_content">
                                            <div class="text">
                                                <p>Они даже материалы нормально закупить не <br> могут!!! Я каждый день им что-то
                                                    <br> дозаказываю и привожу. <img src="img/emoji_3.png" alt=""><img src="img/emoji_3.png" alt="">
                                                    <br>
                                                    Кстати, ты знала, что застройщик нам сдал <br> квартиру с кривыми стенами? А предъявлять
                                                    <br> уже поздно, за свой счет переделываем
                                                </p>
                                            </div>
                                            <div class="message_options">
                                                <p>8.30</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item wow fadeInRight" data-wow-delay="0.15s">
                                    <div class="message_unit reverse">
                                        <div class="layer layer_1"></div>
                                        <div class="layer layer_2"></div>
                                        <div class="layer layer_3 message_content">
                                            <div class="text">
                                                <p>
                                                    Сегодня мне нахамили,  на своем каком-то, <br>
                                                    я не поняла… И пепельницу нашла в ванной, прикинь?! <br>
                                                    На прошлой неделе вообще не вышли без объяснения. Потеряли столько времени,
                                                    <br> ужас, сил нет...
                                                </p>
                                            </div>
                                            <div class="message_options">
                                                <p>11.11</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="troubles_decor_elem _decor_elem_2">
                                <img src="img/troubles_elem_2.png" alt="">
                            </div>
                        </div>

                        <div class="scene_layer page_troubles_scene">
                            <img src="img/doshik_layer.png" alt="" class="layer img_layer _layer_1 wow fadeInUp" >
                            <img src="img/ashtray_layer.png" alt="" class="layer img_layer _layer_2 wow fadeInRight" data-wow-delay="0.15s">
                            <img src="img/glass_layer.png" alt="" class="layer img_layer _layer_3 wow fadeInLeft" data-wow-delay="0.3s">
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="no_repair_block">
            <div class="container">
                <div class="no_repair">
                    <div class="layer_cosiness  wow fadeIn"><img src="img/cosiness.jpg" alt=""></div>
                    <div class="no_repair_inner">
                        <div class="text">
                            <p class="subtitle wow fadeInUp">
                                <span>Нет. Такой ремонт</span>
                                <span>нам не нужен...</span>
                            </p>
                            <h6 class="page_title_big  wow fadeInUp">
                                <b>Вам вообще</b>
                                не нужен <br>
                                ремонт
                            </h6>
                            <p class="wow fadeInUp">Да. Вам нужна квартира, в&nbsp;которой захочется
                                жить, перевезти туда семью, запустить кошечку, открыть бутылку шампанского с&nbsp;любимым человеком, пригласить друзей на&nbsp;новоселье,
                                 а&nbsp;не&nbsp;вот&nbsp;это вот всё.</p>

                        </div>
                        <div class="no_repair_icons_box">
                            <div class="no_repar_icon _yellow  wow fadeInRight">
                                <img src="img/cosiness_icon_1.png" alt="">
                            </div>
                            <div class="no_repar_icon _purple  wow fadeInRight" data-wow-delay="0.15s">
                                <img src="img/cosiness_icon_2.png" alt="">
                            </div>
                            <div class="no_repar_icon _blue  wow fadeInRight" data-wow-delay="0.3s">
                                <img src="img/cosiness_icon_3.png" alt="">
                            </div>
                        </div>
                        <div class="layer_cosiness_mobile">
                            <img src="img/cosiness_mobile.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="welcome_block">
            <div class="container">
                <div class="welcome">
                    <div class="welcome_inner">
                        <p class="page_title_big wow fadeInLeft">
                            Добро пожаловать <br>
                            в ДЗЕН-ДОМ
                        </p>

                        <div class="philosophy_unit wow fadeInLeft">
                            <div class="icon">
                                <img src="img/valter.png" alt="">
                            </div>
                            <div class="text">
                                Наша философия в том, <br>
                                что <b>мы не предлагаем</b> <br>
                                вам ремонт
                            </div>
                        </div>

                        <div class="text wow fadeInLeft">
                            <p>Мы сопровождаем вас в пути от первой задумки до полного принятия работ <br>
                                и заселения.</p>
                            <p>
                                Наша миссия - сделать процесс ремонта безболезненным, комфортным, чем-то <br>
                                незначительным в вашей жизни.
                            </p>
                        </div>

                        <p class="subtitle wow fadeInLeft">·&nbsp;Мне не нужен ремонт!&nbsp;·</p>
                        <div class="btn_wrap wow fadeInLeft">
                            <a href="javascript:void(0)" class="btn_solo btn_get_modal" data-id="#modal_get_order" >
                                <span class="btn_text">
                                    <b>Мне нужна</b> обновленная квартира
                                </span>
                                <span class="btn_solo_icon"></span>
                            </a>
                        </div>

                        <div class="welcome_img_mobile_layer">
                            <img src="img/dzen_people_mobile.png" alt="">
                        </div>

                    </div>
                    <div class="welcome_img_layer wow fadeInRight">
                        <img src="img/dzen_people.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <?php include ("parts/plans_block.php"); ?>

        <section class="only_dzen_remont_block">
            <div class="container">
                <div class="only_dzen_remont only_dzen_remont_box">
                    <div class="only_dzen_remont_item only_dzen_remont_1">
                        <div class="only_dzen_remont_content">
                            <p class="title wow fadeInUp">
                                Почему ремонт с&nbsp;<span class="text_nowrap">ДЗЕН-ДОМ</span> <br> проходит легко и&nbsp;с&nbsp;комфортом
                            </p>

                            <div class="stages_outer">
                                <div class="stage_unit _item_1 wow fadeInLeft">
                                    <div class="stage_unit_title">
                                        <div class="num">01</div>
                                        <div class="title">
                                            Все работы <br> сдаются в срок
                                        </div>
                                    </div>
                                    <div class="stage_unit_text">
                                        <p>
                                            Потому что есть четкий график работ, и строители не могут уйти
                                            с объекта, не выполнив план.
                                        </p>
                                    </div>
                                </div>

                                <div class="stage_unit _item_2  wow fadeInLeft" data-wow-delay="0.15s">
                                    <div class="stage_unit_title">
                                        <div class="num">02</div>
                                        <div class="title">Исключены переделки <br> и&nbsp;исправления</div>
                                    </div>
                                    <div class="stage_unit_text">
                                        <p>Строители ДЗЕН-ДОМ работают строго <br>
                                            по проекту, согласованному с заказчиком. <br> Непопадание в ожидания исключено.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="only_dzen_remont_layer layer_stage_clock">
                            <img src="img/stage_clock.png" alt="" class="wow fadeInUp">
                        </div>

                    </div>

                    <div class="only_dzen_remont_item only_dzen_remont_2">
                        <div class="only_dzen_remont_content">
                            <div class="stage_unit _dark wow fadeInLeft">
                                <div class="stage_unit_title">
                                    <div class="num">03</div>
                                    <div class="title">
                                        Забудьте о&nbsp;конфликтах <br> с&nbsp;рабочими или соседями
                                    </div>
                                </div>
                                <div class="stage_unit_text">
                                    <p>Любые вопросы согласования <br>
                                        с жильцами деликатно решает прораб <br> или менеджер проекта.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="only_dzen_remont_layer layer_stage_pigeon">
                            <img src="img/stage_pigeon.png" alt="" class="wow fadeInDown">
                        </div>
                    </div>

                    <div class="only_dzen_remont_item only_dzen_remont_3">
                        <div class="only_dzen_remont_content">
                            <div class="stage_unit _dark wow fadeInLeft">
                                <div class="stage_unit_title">
                                    <div class="num">04</div>
                                    <div class="title">
                                        Стоимость ремонта <br> остается первоначальной
                                    </div>
                                </div>
                                <div class="stage_unit_text">
                                    <p>
                                        Потому что зафиксирована <br>
                                        в договоре, к которому прикреплена <br> смета на все работы и материалы. <br>
                                        Смета заверяется подписью <br>
                                        и печатью.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="only_dzen_remont_layer layer_stage_pig">
                            <img src="img/stage_pig.png" alt="" class="wow fadeInUp">
                        </div>
                    </div>

                    <div class="only_dzen_remont_item only_dzen_remont_4">
                        <div class="only_dzen_remont_content">
                            <div class="stage_uni wow fadeInLeftt">
                                <div class="stage_unit_title">
                                    <div class="num">05</div>
                                    <div class="title">
                                          Закупка и&nbsp;доставка материалов&nbsp;-
                                        наша забота
                                    </div>
                                </div>
                                <div class="stage_unit_text">
                                    <p>
                                        Отдел снабжения  обеспечит <br> закупку всего необходимого <br> по лучшей цене и&nbsp;доставит до двери.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="only_dzen_remont_layer layer_stage_step_ladder">
                            <img src="img/stage_step_ladder.png" alt="" class="wow fadeInUp">
                        </div>
                    </div>


                    <div class="only_dzen_remont_item only_dzen_remont_5">
                        <div class="bg_layer"><img src="img/stage_bg_flat.jpg" alt=""></div>
                        <div class="only_dzen_remont_content">
                            <div class="stage_unit wow fadeInLeft">
                                <div class="stage_unit_title">
                                    <div class="num">06</div>
                                    <div class="title">
                                        Каждый этап <br>
                                        работ принимает <br>
                                        технадзор
                                    </div>
                                </div>
                                <div class="stage_unit_text">
                                    <p>
                                        Мы стремимся к образцовому <br>
                                        качеству ремонта, поэтому <br>
                                        приглашаем независимого <br>
                                        эксперта на приемку работ
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="only_dzen_remont_layer layer_stage_master">
                            <img src="img/stage_master.png" alt="" class="wow fadeInRight">
                            <img src="img/stage_master_2.png" alt="" class="wow fadeInRight">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page_main_master_block">
            <div class="container">
                <div class="page_main_master">
                    <div class="main_master_layer wow fadeInLeft">
                        <img src="img/page_expert_1.png" alt="" class="_desktop">
                        <img src="img/page_expert_1_mobile.png" alt="" class="_mobile">
                        <div class="label_name_master">
                            <div class="name"><p>Алексей Филютович</p></div>
                            <div class="spec"><p>главный прораб</p></div>
                        </div>
                    </div>
                    <div class="page_main_master_inner">
                        <p class="page_title_middle main_master_title wow fadeInLeft">
                            Хотите раз и навсегда <br>
                            снять <b>все вопросы</b><br>
                            с ремонтом?
                        </p>

                        <div class="just_call_master_text wow fadeInRight">
                            <p>Оставьте заявку на бесплатный выезд прораба</p>
                            <div class="btn_wrap">
                                <a href="javascript:void(0)" class="btn_solo btn_get_modal" data-id="#modal_get_order">
                                <span class="btn_text">
                                    <b>Оставить заявку</b>
                                </span>
                                    <span class="btn_solo_icon"></span>
                                </a>
                            </div>
                        </div>
                        <div class="main_master_note wow fadeInRight">
                            <p>
                                Прораб произведет необходимые замеры <br> и&nbsp;проконсультирует по всем вопросам ремонта
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="differences_block">
            <div class="container">
                <div class="differences">
                    <p class="page_title wow fadeInUp">
                        И чем <b>мы отличаемся</b> от множества <br>
                        ремонтных компаний и частных бригад?
                    </p>

                    <div class="differences_outer">
                        <div class="differences_elem difference_stage_elem_1">
                            <div class="difference_stage_unit  wow fadeInUp">
                                <div class="num">01</div>
                                <div class="content">
                                    <p class="title">
                                        Высочайший сервис <br>
                                        и забота о клиенте
                                    </p>
                                    <p class="subtitle">
                                        вместо волокиты и необходимости <br>
                                        постоянной ревизии
                                    </p>
                                </div>
                            </div>
                            <div class="difference_stage_text  wow fadeInUp ">
                                <p>Весь процесс построен так, что вы не тратите
                                    свои нервы на контроль нашего труда. Мы беспокоим
                                    вас лишь в случае крайней необходимости, например, приглашаем на приемку работ.</p>
                            </div>
                            <div class="i_tech_outer  wow fadeInUp">
                                <p class="tech_title">Нет нужды каждый день приезжать на&nbsp;квартиру, потому&nbsp;что <br> о&nbsp;ходе ремонта можно узнать благодаря:</p>
                                <div class="i_tech_box">
                                    <div class="item">
                                        <div class="i_tech_unit">
                                            <div class="icon"><img src="img/repair_tech_1.jpg" alt=""></div>
                                            <div class="text">
                                                <p>Видеонаблюдению <br>
                                                    в режиме онлайн
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="i_tech_unit _center">
                                            <div class="icon"><img src="img/repair_tech_2.jpg" alt=""></div>
                                            <div class="text">
                                                <p>Ежедневным <br>
                                                    фотоотчетам
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="i_tech_unit">
                                            <div class="icon"><img src="img/repair_tech_3.jpg" alt=""></div>
                                            <div class="text">
                                                <p>Общему чату <br>
                                                    с менеджером проекта <br>
                                                    и прорабом
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="hand_mobile_layer">
                                <div class="flat_bg_layer"><img src="img/green_bg_flat.jpg" alt=""></div>
                                <div class="hand_layer"><img src="img/hand_phone.png" alt="" class=" wow fadeInRight"></div>
                            </div>
                        </div>

                        <div class="differences_elem_outer_2_3">
                            <div class="differences_elem difference_stage_elem_2">
                                <div class="inner_content">
                                    <div class="difference_stage_unit wow fadeInLeft">
                                        <div class="num">02</div>
                                        <div class="content">
                                            <p class="title">
                                                Безупречное <br>
                                                качество ремонта
                                            </p>
                                            <p class="subtitle">
                                                вместо попыток тщательно <br>
                                                скрытых дефекты
                                            </p>
                                        </div>
                                    </div>
                                    <div class="difference_stage_text">
                                        <p>
                                            Мы пригласим вас на приемку только, когда прораб <br> лично убедится в безукоризненности выполненных
                                            <br> работ.
                                        </p>
                                        <p>
                                            Только, когда проверит толщину шпатлевки, <br> величину зазоров, ровность стен, качество укладки <br> плитки, чистоту в квартире и еще много всего
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="differences_elem difference_stage_elem_3">
                                <div class="img_layer_blur_lamp">
                                    <img src="img/blur_lamp.png" alt="">
                                </div>
                                <div class="inner_content">
                                    <div class="difference_stage_unit wow fadeInLeft">
                                        <div class="num">03</div>
                                        <div class="content">
                                            <p class="title">
                                                Выгодная закупка <br>
                                                под ключ
                                            </p>
                                            <p class="subtitle">
                                                Вместо самостоятельного поиска <br>
                                                скидок и предложений
                                            </p>
                                        </div>
                                    </div>
                                    <div class="difference_stage_text">
                                        <p>Отдел снабжения гарантированно добьется для вас лучшей цены у поставщиков.</p>
                                        <p>Максимально низкая цена на черновые материалы и очень-очень вкусные бонусы в магазинах чистовых материалов (светильники, двери, сантехника).</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="differences_elem difference_stage_elem_4 ">
                            <div class="img_layer_tech_station">
                                <img src="img/tech_station.png" alt="" class="wow fadeInUp">
                            </div>
                            <div class="inner_content">
                                <div class="difference_stage_unit wow fadeInLeft">
                                    <div class="num">04</div>
                                    <div class="content">
                                        <p class="title">
                                            Технологичность <br>
                                            процесса
                                        </p>
                                        <p class="subtitle">
                                            вместо устаревших методов <br>
                                            работы и коммуникаций
                                        </p>
                                    </div>
                                </div>
                                <div class="difference_stage_text wow fadeInLeft">
                                    <p>
                                        В вашей квартире мы используем самые современные <br> инструменты, штукатурные станции, лазерные дальномеры, <br> влагомеры и другие новинки техники.
                                    </p>
                                    <p>
                                        Используем передовую CRM-систему, доработанную нами <br> под нужды наших клиентов. Готовим релиз мобильного
                                        <br> приложения для максимального удобства заказчика.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="excursion_block">
            <div class="container">
                <div class="excursion">
                    <div class="img_layer_painter_master">
                        <img src="img/painter_master.png" alt="" class="_desktop wow fadeInRight">
                        <img src="img/painter_master_mobile.png" alt="" class="_mobile wow fadeInRight">
                    </div>
                    <div class="excursion_inner wow fadeInLeft">
                        <p class="page_title">
                            Хотите увидеть <b>как мы работаем?</b> <br>
                            Посмотреть квартиры, которые уже <br>
                            закончены или еще в процессе?
                        </p>
                        <p class="page_subtitle">Запишитесь на экскурсию по объектам</p>
                        <form class="page_form send_form">
                            <div class="inputs_box">
                                <div class="item">
                                    <label class="input"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
                                </div>
                                <div class="item">
                                    <label class="input"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                                </div>
                            </div>
                            <div class="btn_wrap">
                                <button class="btn_submit btn_light" ><span class="btn_text">Записаться</span></button>
                            </div>
                            <input name="input_from" type="hidden" value="Запишитесь на экскурсию по объектам">
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include "parts/team_block.php"; ?>

        <section class="internal_control_block">
            <div class="container">
                <div class="internal_control">

                    <div class="internal_control_box_1">
                        <div class="item item_1">
                            <div class="internal_control_elem_outer_1">
                                <div class="internal_title_box wow fadeInDown">
                                    <div class="title">
                                        <p class="page_title_middle">
                                            <b>Мы не пустим</b> <br>
                                            кого попало <br>
                                            в ваш дом
                                        </p>
                                    </div>
                                    <div class="text">
                                        <p>Каждый сотрудник проходит <br> <b>многошаговый отбор</b> <br> перед тем, как попасть <br> к нам</p>
                                    </div>
                                </div>
                                <div class="internal_control_elem internal_control_elem_1 wow fadeInLeft">
                                    <div class="internal_control_elem_inner">
                                        <h6 class="title">
                                            Проверяем <br>
                                            документы <br>
                                            и знания
                                        </h6>
                                        <p>
                                            Обязательно наличие патентов
                                            и разрешений на работу, владение русским языком. Задаем вопросы
                                            по специальности и теоретические задачи.
                                        </p>

                                    </div>
                                    <div class="img_layer img_layer_internal_control_1">
                                        <img src="img/internal_control_1.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="item item_2 wow fadeInRight">
                            <div class="internal_control_elem internal_control_elem_2 _light">
                                <div class="internal_control_elem_inner">
                                    <h6 class="title">Знакомимся <br>  с работами <br>  кандидатов</h6>
                                    <p>Просим показать квартиры, которые они делали, и смотрим качество работ. Запрашиваем отзывы и
                                        рекомендации хозяев квартир и предыдущих работодателей.</p>
                                </div>
                                <div class="img_layer img_layer_internal_control_2">
                                    <img src="img/internal_control_2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="internal_control_box_2 ">
                        <div class="item item_1 wow fadeInLeft">
                            <div class="internal_control_elem internal_control_elem_3 _light">
                                <div class="internal_control_elem_inner">
                                    <h6 class="title">Даем тестовое <br> задание</h6>
                                    <p>Отправляем на один из наших объектов вместе с опытными строителями. Получаем заключение от прораба по итогам тестового периода и принимаем решение
                                        о приеме соискателя на работу.</p>
                                </div>
                                <div class="img_layer img_layer_internal_control_3">
                                    <img src="img/internal_control_3.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item item_2 wow fadeInRight">
                            <div class="internal_control_elem internal_control_elem_4">
                                <div class="internal_control_elem_inner">
                                    <h6 class="title">Регулярные <br> аттестации <br>  и обучение</h6>
                                    <p>Все сотрудники проходят периодическую аттестацию и курсы повышения квалификации.  <br>
                                        Мы быстро увольняем сотрудников,
                                        чье качество работ неприемлемо
                                        для компании, ориентированной
                                        на клиента.
                                    </p>
                                </div>
                                <div class="img_layer img_layer_internal_control_4">
                                    <img src="img/internal_control_4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="own_principle">
                        <div class="own_principle_inner wow fadeInLeft">
                            <h6 class="page_title_middle">
                                Долго <b>выбирать <br>
                                лучших</b> и быстро <br>
                                увольнять криворуких –
                            </h6>
                            <p>принцип построения команды мастеров ДЗЕН-ДОМ</p>
                        </div>
                        <div class="img_layer stars_hand_layer wow fadeInUp">
                            <img src="img/stars_hand.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="super_team_block">
            <div class="bg_layer_super_team">
                <img src="img/bg_super_team.jpg" alt="" class="_desktop">
                <img src="img/bg_super_team_mobile.jpg" alt="" class="_mobile">
            </div>
            <div class="container">
                <div class="super_team">
                    <div class="super_team_layer wow fadeInLeft">
                        <img src="img/super_team.png" alt="" class="_desktop">
                        <img src="img/super_team_mobile.png" alt="" class="_mobile">

                        <div class="label_name_master label_name_team_1">
                            <div class="name"><p>Михаил Копылов</p></div>
                            <div class="spec"><p>прораб</p></div>
                        </div>

                        <div class="label_name_master label_name_team_2">
                            <div class="name"><p>Жанна Яновская</p></div>
                            <div class="spec"><p>менеджер проектов</p></div>
                        </div>

                        <div class="label_name_master label_name_team_3">
                            <div class="name"><p>Денис Правдинский</p></div>
                            <div class="spec"><p>руководитель отдела качества</p></div>
                        </div>
                    </div>
                    <div class="super_team_inner wow fadeInRight">
                        <p class="page_title super_team_title">
                            Может, <b>познакомимся</b> <br>
                            с нашей командой? <br>
                            Запишитесь на встречу <br>
                            в офисе.
                        </p>
                        <p class="page_subtitle">Такси до&nbsp;офиса и&nbsp;обратно за&nbsp;наш&nbsp;счет</p>

                        <form class="page_form send_form">
                            <div class="input_item">
                                <label class="input"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
                            </div>
                            <div class="input_item">
                                <label class="input"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                            </div>
                            <div class="btn_wrap">
                                <button class="btn_submit btn_light" ><span class="btn_text">Записаться</span></button>
                            </div>
                            <input name="input_from" type="hidden" value="Запишитесь на встречу в офисе">
                        </form>


                    </div>
                </div>
            </div>
        </section>


        <section class="work_process_block">
            <div class="container">
                <div class="work_process">
                    <p class="page_title page_process_title wow fadeInUp">Развитие сюжета <br> <b>отлажено как часы</b></p>
                    <p class="page_process_subtitle wow fadeInUp">Нажмите на цифру, чтобы прочесть об этапе</p>

                    <div class="process_clock_box">
                        
                        <div class="clock_outer_layer wow fadeIn">
                            <img src="img/clock.jpg" alt="" class="_desktop">
                            <img src="img/clock_mobile.jpg" alt="" class="_mobile">
                        </div>
                        <div class="clock_tips">
                            <div class="item clock_tip_unit _item_1 wow fadeInRight">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>Вы получаете <b>консультацию <br>
                                            специалиста</b> сразу после <br>
                                        заявки
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_2 wow fadeInRight">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>Прораб выезжает на квартиру <br>
                                        <b>для замеров</b> и понимания <br>
                                        задачи
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_3 wow fadeInRight">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p><b>Составляем договор</b>, прикрепляем <br>
                                        смету на материалы, работы <br>
                                        и заявку на проведение работ.
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_4 wow fadeInRight">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p><b>Составляем график работ</b>, <br>
                                        технический проект и рабочую <br>
                                        документацию.</p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_5 wow fadeInUp">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>Закупаем материалы <b>по самой <br>
                                            выгодной цене</b> и доставляем <br>
                                        в квартиру
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_6 wow fadeInLeft">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>Строители <b>поэтапно <br>
                                            выполняют  работы</b> согласно <br>
                                        проекту и графику
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_7  wow fadeInLeft">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p><b>Вы принимаете все этапы</b> <br>
                                        совместно с независимым <br>
                                        технадзором
                                    </p>
                                </div>
                            </div>

                            <div class="item clock_tip_unit _item_8  wow fadeInLeft">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>
                                        Проводим уборку, вывозим мусор, <br>
                                        заказываем <b>послестроительный <br>
                                            клининг</b> за свой счет
                                    </p>
                                </div>
                            </div>
                            <div class="item clock_tip_unit _item_9  wow fadeInLeft">
                                <div class="clock_close"></div>
                                <div class="text">
                                    <p>Вручаем вам ключи и подарки <br>
                                        Вы принимаете чистую квартиру <br>
                                        <b>готовую к заселению</b>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="clock_nums">
                            <div class="item clock_num_unit _item_1">I</div>
                            <div class="item clock_num_unit _item_2">II</div>
                            <div class="item clock_num_unit _item_3">III</div>
                            <div class="item clock_num_unit _item_4">IV</div>
                            <div class="item clock_num_unit _item_5">V</div>
                            <div class="item clock_num_unit _item_6">VI</div>
                            <div class="item clock_num_unit _item_7">VII</div>
                            <div class="item clock_num_unit _item_8">VII</div>
                            <div class="item clock_num_unit _item_9">IX</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="predesign_block">
            <div class="container">
                <div class="predesign">
                    <div class="master_architect_layer wow fadeInRight">
                        <img src="img/expert_man.png" alt="">
                        <div class="label_name_master">
                            <div class="name"><p>Федор Кирсон</p></div>
                            <div class="spec"><p>архитектор ДЗЕН-ДОМ</p></div>
                        </div>
                    </div>
                    <div class="predesign_spot_light_layer">
                        <img src="img/spot_light.png" alt="">
                    </div>
                    <div class="predesign_inner wow fadeInLeft">
                        <p class="page_title_middle">
                            У вас уже есть <br>
                            <b>дизайн-проект</b> <br>
                            или смета на работы?
                        </p>
                        <p class="page_predesign_subtitle">Мы посчитаем стоимость реализации проекта <br>
                            или оптимизируем вашу смету</p>

                        <form class="page_form send_form">
                            <div class="inputs_box">
                                <div class="item">
                                    <label class="input"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
                                </div>
                                <div class="item">
                                    <label class="input"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                                </div>
                            </div>
                            <div class="btn_wrap">
                                <button class="btn_submit btn_light"><span class="btn_text">Получить смету на работы</span></button>
                            </div>
                            <input type="hidden" name="input_from" value="Получить смету на работы">
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="help_planning_block">
            <div class="container">
                <div class="help_planning">
                    <div class="help_planning_inner  wow fadeInLeft">
                        <p class="page_title ">
                            <b>Согласовать</b> перепланировку <br>
                            или <b>заранее выбрать</b> квартиру <br>
                            с подходящей планировкой?
                        </p>
                        <p class="page_subtitle">Мы поможем в обоих случаях. Отправьте заявку, и расскажем как.</p>

                        <form class="page_form send_form">
                            <div class="inputs_box">
                                <div class="item">
                                    <label class="input"><input type="text" name="name" placeholder="Как вас зовут?" class="req"></label>
                                </div>
                                <div class="item">
                                    <label class="input"><input type="tel" name="phone" placeholder="Ваш телефон" class="req"></label>
                                </div>
                            </div>
                            <div class="btn_wrap">
                                <button class="btn_submit btn_light"><span class="btn_text">Записаться</span></button>
                            </div>
                            <input type="hidden" name="input_from" value="Поможем согласовать перепланировку или заранее выбрать квартиру с подходящей планировкой">
                        </form>
                    </div>
                    <div class="img_layer_planning wow fadeInUp">
                        <img src="img/planning.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="only_pleasure_block">
            <div class="container">
                <div class="only_pleasure">
                    <div class="layer_tour_city">
                        <img src="img/bg_family_tour2.jpg" alt="" class="_desktop">
                        <img src="img/bg_family_tour_mobile.jpg" alt="" class="_mobile">
                    </div>
                    <div class="layer_family_tour">
                        <img src="img/family_tour.png" alt="" class="_desktop wow fadeInRight">
                        <img src="img/family_tour_mobile.png" alt="" class="_mobile wow fadeInRight">
                    </div>
                    <div class="part_bg_title"></div>
                    <div class="only_pleasure_inner">
                        <p class="page_title only_pleasure_title wow fadeInLeft">А если бы Саша <br>
                            и Алина сразу <br>
                            обратились в ДЗЕН-ДОМ:</p>

                        <div class="only_pleasure_box">
                            <div class="item_1 only_pleasure_outer_1">
                                <div class="only_pleasure_unit _unit_1 wow fadeInLeft">
                                    <div class="num">01</div>
                                    <div class="text">
                                        <p>
                                            <b>Они могли бы уехать <br>
                                            в отпуск,</b> а не бояться, <br>
                                            что ремонт станет <br>
                                            дороже
                                        </p>
                                    </div>
                                </div>
                                <div class="img_airplaine_layer">
                                    <img src="img/airplane.png" alt="" class="wow fadeInUp">
                                </div>
                            </div>
                            <div class="item_2">
                                <div class="only_pleasure_outer_2">
                                    <div class="only_pleasure_unit _unit_2 _light wow fadeInLeft">
                                        <div class="num">02</div>
                                        <div class="text">
                                            <p>
                                                <b>Встречаться с&nbsp;друзьями,</b> <br>
                                                а&nbsp;не&nbsp;спорить с&nbsp;неумелыми <br>
                                                ремонтниками.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="only_pleasure_outer_3">
                                    <div class="only_pleasure_unit _unit_3 wow fadeInLeft">
                                        <div class="num">03</div>
                                        <div class="text">
                                            <p>
                                                <b>Ходить в&nbsp;кино,</b> <br>
                                                а&nbsp;не&nbsp;проводить выходные <br>
                                                в&nbsp;строительных магазинах <br>
                                                дороже
                                            </p>
                                        </div>
                                    </div>
                                    <div class="img_popcorn_layer wow fadeInDown">
                                        <img src="img/popcorn.png" alt="">
                                    </div>
                                </div>
                                <div class="only_pleasure_outer_4">
                                    <div class="only_pleasure_unit _unit_4 _light wow fadeInLeft">
                                        <div class="num">04</div>
                                        <div class="text">
                                            <p>
                                                <b>Жить как им хочется,</b> <br>
                                                не&nbsp;тратя силы и&nbsp;нервы <br>
                                                на сложный и&nbsp;изнуряющий <br>
                                                процесс ремонта
                                            </p>
                                        </div>
                                    </div>
                                    <div class="img_spot_light_part">
                                        <img src="img/sun_spot_part_layer.png" alt="">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="last_block">
            <div class="container">
                <div class="last">
                    <div class="tour_place_layer">
                        <img src="img/tour_place.jpg" alt="" class="_desktop wow fadeInRight">
                        <img src="img/tour_place_mobile.png" alt="" class="_mobile wow fadeInRight">
                    </div>
                    <div class="last_inner">
                        <p class="page_title_last wow fadeInLeft">Вам <b>нужен ремонт</b> со всеми <br>
                            вытекающими или <b>квартира,</b> <br>
                            <b>готовая</b> к заселению?</p>

                        <p class="page_subtitle_last wow fadeInLeft">Чистая, уютная, светлая, в которой <br>
                            пахнет новизной?</p>

                        <div class="last_note_box">
                            <div class="item item_note item_1 wow fadeInLeft">
                                <p>Оставьте заявку на бесплатный выезд прораба <br>
                                    для проведения замеров и оценки объема работ.
                                </p>
                            </div>
                            <div class="item_divider wow fadeInLeft"></div>
                            <div class="item item_note item_2 wow fadeInLeft">
                                <p>Гарантируем, вы получите обновленное <br>
                                    жилье в оговоренные сроки и бюджет.
                                </p>
                            </div>
                        </div>

                        <div class="btn_wrap wow fadeInLeft">
                            <a href="#" class="btn_light btn _yellow btn_get_modal" data-id="#modal_get_order">
                                <span class="btn_text">Мне нужна обновленная&nbsp;квартира</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <footer class="footer_block">
            <div class="container">
                <div class="footer">
				
					<div class="footer_box">
                        <div class="footer_side_1">
                            <div class="footer_logo">
                                <img src="img/logo2.jpg" alt="">
                            </div>
                            <div class="footer_address">
								<p><?php echo $city['address'];?></p>
                            </div>
                        </div>
                        <div class="footer_side_2 footer_conf">
                            <p>
                                <a href="documents/Intellektualnaya_sobstvennost_DZEN_DOM.pdf" target="_blank">Копирование элементов сайта запрещено</a>  <br>  Законом&nbsp;РФ "Об&nbsp;авторском&nbsp;праве и&nbsp;смежных&nbsp;правах"
                            </p>
                            <p>
                                <a href="documents/site_info.pdf" target="_blank">Правовая информация</a>
                            </p>
                            <p>
                                Создание сайта: <a href="https://vk.com/dpravdinskiy" target="_blank">Денис Правдинский</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php include "parts/quiz.php";?>
    </div>
</div>
<?php include "parts/modals.php";?>
<?php include "parts/footer.php";?>

</body>
</html>
